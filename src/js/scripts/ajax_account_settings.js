jQuery(document).ready(function ($) {

    $('#ajax_account_settings .input-col').wrapInner('<div class="input"></div>');
    $('#ajax_account_settings .input-col .input').append('<div class="error_message"></div>');

    function field_validation(field, pattern) {
        let parent = field.closest('.input');
        if (field.val() == '') {
            parent.addClass('wrong');
            parent.find('.error_message').text(field.attr('data-empty'));
        } else if (!pattern.test(field.val())) {
            parent.addClass('wrong');
            parent.find('.error_message').text(field.attr('data-wrong'));
        } else {
            parent.removeClass('wrong');
            parent.find('.error_message').text('');
        }
    }

    function clear(form) {
        form.find('.input').each(function () {
            $(this).removeClass('wrong');
            $(this).find('.error_message').text('');
        });
    }

    $('#ajax_account_settings').submit(function () {
        let form = $(this);

        clear(form);

        let name = $(this).find('input[type="text"].name').first();
        let lastname = $(this).find('input[type="text"].lastname').first();

        field_validation(name, new RegExp(/^[a-z ,.'-]{2,}$/i));
        field_validation(lastname, new RegExp(/^[a-z ,.'-]{2,}$/i));

        let errors = $(this).find('.input.wrong');
        if (errors.length > 0) return false;

        let form_data = new FormData();

        form.find(':not([type="file"])').each(function () {
            form_data.append($(this).attr('name'), $(this).val());
        });
        let file_data = $('.change_avatar').prop('files')[0];
        form_data.append('file', file_data);
        // form_data.append('name', name);
        // form_data.append('lastname', lastname);
        form_data.append('action', 'ajax_account_settings');

        $.ajax({
            type: 'post',
            url: theme.ajaxurl,
            dataType: 'json',
            data: form_data,
            // cache: false,
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
            beforeSend: function () {

            },
            success: function (response) {
                if (response.state == true) {
                    location.reload();
                } else {
                    alert(response.message);
                }

            },
            error: function (response) {
                console.log(response);
            }
        });

        return false;
    });

});
