jQuery(document).ready(function($) {

  function field_validation(field) {
    let parent = field.closest('.input');
    if(field.val() == '') {
    	parent.addClass('wrong');
    	parent.find('.error_message').text(field.attr('data-empty'));
    } else {
      parent.removeClass('wrong');
      parent.find('.error_message').text('');
    }
  }
  function clear(form) {
  	form.find('.input').each(function() {
      $(this).removeClass('wrong');
      $(this).find('.error_message').text('');
  	});
  }

  $('#ajax_question_editor').submit(function() {
  	let form = $(this);

  	clear(form);

  	let title = $(this).find('.title_input').first();
  	let categories = $(this).find('.category_input:checked');
  	let text = $(this).find('#question_textarea').first();

  	let is_single = $(this).find('input[name="is_single"]').first().val();
  	let is_new_post = $(this).find('input[name="post_id"]').first().val();
  	is_new_post = is_new_post == 0 ? true : false;

  	let message = '';

  	if(title.val() == '' || text.val() == '' || categories.length < 1) {
  		message = 'All fields are required.';
  	} else if(categories.length >5) {
  		message = 'You can insert maximum 5 categories.';
  	}

  	if(message != '') {
			$.fancybox.open(
				'<div class="message">' + message + '</div>',
				{
					smallBtn : true,
					toolbar : false
				}
			);

			return false;
  	}

    let form_data = $(this).serializeArray();

    form_data.push({name: 'action', value: 'ajax_question_editor'});

    $.ajax({
      type: 'post',
      url: theme.ajaxurl,
      dataType: 'json',
      data: form_data,
      beforeSend: function () {

      },
      success: function (response) {
				if (response.state == true) {
					$('#question_editor_success_popup .text-col').text(response.message);
					$('#question_editor_popup .title').text('Edit question');
					$('#question_editor_popup input[name="post_id"]').val(response.post_id);
					if(!is_new_post) {
						if(is_single) {
							$('#article_body').replaceWith(response.article);
						} else {
							$('article.post-id-' + response.post_id).replaceWith(response.article);
						}
					} else {
						$('#articles').prepend(response.article);
					}
                    $.fancybox.close(true);
					$.fancybox.open({
						src : '#question_editor_success_popup',
                        baseClass : 'bg-no',
                        hideScrollbar : false,
					});
        } else {
        	alert(response.message);
        }

      },
      error: function (response) {
        console.log(response);
      }
    });

  	return false;
  });


});
