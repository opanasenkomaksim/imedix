jQuery(document).ready(function($) {

	$('#ajax_new_password .input-col').wrapInner('<div class="input"></div>');
	$('#ajax_new_password .input-col .input').append('<div class="error_message"></div>');

  function clear(form) {
  	form.find('.input').each(function() {
      $(this).removeClass('wrong');
      $(this).find('.error_message').text('');
  	});
  }

  $('#ajax_new_password').submit(function() {
  	let form = $(this);

  	clear(form);

  	let password = $(this).find('input[type="password"]').first();
  	let password2 = $(this).find('input[type="password"]').last();

    let parents = password.closest('.input').add(password2.closest('.input'));
    let parent2 = password2.closest('.input');
    if(password.val() == '' || password2.val() == '') {
    	parents.addClass('wrong');
    	parent2.find('.error_message').text(password2.attr('data-empty'));
    } else if (password.val() != password2.val()) {
      parents.addClass('wrong');
      parent2.find('.error_message').text(password2.attr('data-wrong'));
    }

  	let errors = $(this).find('.input.wrong');

  	if(errors.length > 0) return false;

    let form_data = $(this).serializeArray();

    form_data.push({name: 'action', value: 'ajax_new_password'});

    $.ajax({
      type: 'post',
      url: theme.ajaxurl,
      dataType: 'json',
      data: form_data,
      beforeSend: function () {

      },
      success: function (response) {
				if (response.state == true) {
					clear(form);
					$('#ajax_new_password').find('.message').html(response.message);
        } else {
        	alert(response.message);
        }

      },
      error: function (response) {
        console.log(response);
      }
    });

  	return false;
  });

});
