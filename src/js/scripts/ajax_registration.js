jQuery(document).ready(function($) {

	$('.ajax_registration .input-col').wrapInner('<div class="input"></div>');
	$('.ajax_registration .input-col .input').append('<div class="error_message"></div>');

  function field_validation(field, pattern) {
    let parent = field.closest('.input');
    if(field.val() == '') {
    	parent.addClass('wrong');
    	parent.find('.error_message').text(field.attr('data-empty'));
    } else if (!pattern.test(field.val())) {
      parent.addClass('wrong');
      parent.find('.error_message').text(field.attr('data-wrong'));
    } else {
      parent.removeClass('wrong');
      parent.find('.error_message').text('');
    }
  }
  function clear(form) {
  	form.find('.input').each(function() {
      $(this).removeClass('wrong');
      $(this).find('.error_message').text('');
  	});
  }

  $('.ajax_registration').submit(function() {
  	let form = $(this);

  	clear(form);

  	let email = $(this).find('input[type="email"]').first();
  	let name = $(this).find('input[type="text"].name').first();
  	let lastname = $(this).find('input[type="text"].lastname').first();
  	let password = $(this).find('input[type="password"]').first();
  	let all = $(this).find('input[type="email"], input[type="password"], input[type="text"]');

  	field_validation(email, new RegExp(/^[A-Z0-9\._-]{2,30}@[A-Z0-9\._-]{2,10}\.[A-Z]{2,4}$/i));
  	field_validation(name, new RegExp(/^[a-z ,.'-]{2,}$/i));
  	field_validation(lastname, new RegExp(/^[a-z ,.'-]{2,}$/i));
  	field_validation(password, new RegExp("^.{6,}$"));

  	let emptyInputs = all.filter(function() { return this.value == ""; });
  	let errors = $(this).find('.input.wrong');
  	if(emptyInputs.length > 0 || errors.length > 0) return false;

    let form_data = $(this).serializeArray();

    form_data.push({name: 'action', value: 'ajax_registration'});

    $.ajax({
      type: 'post',
      url: theme.ajaxurl,
      dataType: 'json',
      data: form_data,
      beforeSend: function () {

      },
      success: function (response) {
				if (response.state == true) {
					$('#confirm_email_popup .email_confirmation_link').attr('data-email', response.email);
					$.fancybox.open({
						src : '#confirm_email_popup'
					});
        } else {
        	alert(response.message);
        }

      },
      error: function (response) {
        console.log(response);
      }
    });

  	return false;
  });

});
