jQuery(document).ready(function($) {

	$('.edit_question_link').click(function() {
		if($('#question_editor_popup input[name="post_id"]').val() == $(this).attr('href')) {
			$.fancybox.open({
				src : '#question_editor_popup'
			});

			return false;
		}

  	let link = $(this);
    let form_data = [];

    form_data.push({name: 'post_id', value: $(this).attr('href')});
    form_data.push({name: 'nonce', value: $(this).attr('data-nonce')});
    form_data.push({name: 'action', value: 'ajax_get_question'});

    $.ajax({
      type: 'post',
      url: theme.ajaxurl,
      dataType: 'json',
      data: form_data,
      beforeSend: function () {

      },
      success: function (response) {
				if (response.state == true) {
					$('#question_editor_popup .title_input').val(response.title);
					$('#question_editor_popup #question_textarea').summernote('code', response.content);
					$('#question_editor_popup .category_input').each(function() {
						if($.inArray(parseInt($(this).val()), response.categories) !== -1 && !$(this).prop('checked')) {
							$(this).trigger('click');
						} else if($.inArray(parseInt($(this).val()), response.categories) === -1 && $(this).prop('checked')) {
							$(this).trigger('click');
						}
					});
					$('#question_editor_popup .title').text('Edit question');
					$('#question_editor_popup input[name="post_id"]').val(response.post_id);

					$.fancybox.open({
						src : '#question_editor_popup',
						opts : {
							afterShow : function( instance, current ) {
								let object = $('.multiselect');
								let length = 0;
								object.find('.item').each(function() {
									length = length + $(this).outerWidth(true);
								});
								let display = object.find('.display').first();
								let search = object.find('.search').first();
								let width = display.width();
								if(length / width <= 1) {
									width = width - length;
								} else {
									width = length % width;
								}
								search.css({'width': '50px'});
								search.width(Math.max(width - search.css('padding-left').replace("px", "") - search.css('padding-right').replace("px", ""), search.width()));
						  }
						}
					});
        } else {
        	alert(response.message);
        }

      },
      error: function (response) {
        console.log(response);
      }
    });

		return false;
	});


});
