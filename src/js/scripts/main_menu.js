import $ from "jquery";

jQuery(document).ready(function ($) {

    $('#main_menu > .menu-item > a').append('<span class="effect"></span>');

    $('#main_menu a, #main_menu span').click(function (e) {
        let menu = $(this).siblings('.sub-menu-wrap');
        let menu_item = $(this).closest('.menu-item');
        let menu_items = menu_item.siblings('.menu-item.active');

        menu_items.each(function () {
            $(this).removeClass('active');
            $(this).children('.sub-menu-wrap.show').removeClass('show');
        });

        if (menu.length == 1) {
            let submenu = menu.first();
            submenu.toggleClass('show');
            menu_item.toggleClass('active');

            // return false;
        }
    });

    $('#main_menu > .menu-item').hover(function (e) {
        if (viewport().width >= 992) {
            let submenu = $(this).find('.sub-menu-wrap').first();
            if (submenu.length !== 0) {
                $(this).addClass('active');
                submenu.addClass('show');
            }
        }
    }, function (e) {
        if (viewport().width >= 992) {
            let submenu = $(this).find('.sub-menu-wrap').first();
            if (submenu.length !== 0) {
                $(this).removeClass('active');
                submenu.removeClass('show');
            }
        }
    });

    menu_classes();
    $(window).resize(function () {
        menu_classes();
        header_styles();
    });

    $(document).on("click", function (event) {
        // If the target is not the container or a child of the container, then process
        // the click event for outside of the container.
        if (viewport().width < 992) {
            if ($(event.target).closest("#main_menu_wrapper").length === 0) {
                $('#main_menu_wrapper').collapse('hide');
            }
        }
    });

    header_styles();
    $(document).scroll(function() {
        header_styles();
    });

});

function menu_classes() {
    if (viewport().width < 992) {
        $('#navbarNav').addClass('mobile');
        $('#navbarNav').removeClass('desktop');
    } else {
        $('#navbarNav').removeClass('mobile');
        $('#navbarNav').addClass('desktop');
    }
}

function header_styles() {
    if($(document).scrollTop() > 0 && !$('#navigation').hasClass('fixed')) {
        $('#navigation').addClass('fixed');
    } else if($(document).scrollTop() == 0 && $('#navigation').hasClass('fixed')) {
        $('#navigation').removeClass('fixed');
    }

    let height = $('#wpadminbar').length > 0 ? $('#wpadminbar').height() : 0;

    if(viewport().width <= 600) {
        if($('#navigation').hasClass('fixed')) {
            $('body.admin-bar #navigation').css({marginTop: - height + 'px'});
        } else {
            $('body.admin-bar #navigation').css({marginTop: 0});
        }
    } else {
        $('body.admin-bar #navigation').css({marginTop: 0});
    }
}
