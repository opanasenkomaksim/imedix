jQuery(document).ready(function($) {

  $(document).on('click', '.email_confirmation_link', function() {

  	let link = $(this);
    let form_data = [];

    form_data.push({name: 'email', value: $(this).attr('data-email')});
    form_data.push({name: 'action', value: 'ajax_email_confirmation'});

    $.ajax({
      type: 'post',
      url: theme.ajaxurl,
      dataType: 'json',
      data: form_data,
      beforeSend: function () {

      },
      success: function (response) {
				if (response.state == true) {
					if(link.closest('#confirm_email_popup').length !== 1) {
						$('#confirm_email_popup .email_confirmation_link').attr('data-email', link.attr('data-email'));
						$.fancybox.open({
							src : '#confirm_email_popup'
						});
					}
        } else {
        	alert(response.message);
        }

      },
      error: function (response) {
        console.log(response);
      }
    });

  	return false;
  });

});
