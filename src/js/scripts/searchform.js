jQuery(document).ready(function($) {

	$('.search-form input[type="search"]').focus(function() {
		$(this).closest('.search-form').addClass('focus');
	}).blur(function() {
		$(this).closest('.search-form').removeClass('focus');
	});
	$('.search-form').append('<div class="clear"></div>');
	$(document).on('mousedown', '.search-form .clear', function() {
	    return false;
	}).on('click', '.search-form .clear', function() {
		$(this).closest('.search-form').find('input[type="search"]').val('');
	});
});
