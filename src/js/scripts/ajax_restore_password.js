jQuery(document).ready(function($) {

	$('#ajax_restore_password .input-col').wrapInner('<div class="input"></div>');
	$('#ajax_restore_password .input-col .input').append('<div class="error_message"></div>');

  function field_validation(field, pattern) {
    let parent = field.closest('.input');
    if(field.val() == '') {
    	parent.addClass('wrong');
    	parent.find('.error_message').text(field.attr('data-empty'));
    } else if (!pattern.test(field.val())) {
      parent.addClass('wrong');
      parent.find('.error_message').text(field.attr('data-wrong'));
    } else {
      parent.removeClass('wrong');
      parent.find('.error_message').text('');
    }
  }
  function clear(form) {
  	form.find('.input').each(function() {
      $(this).removeClass('wrong');
      $(this).find('.error_message').text('');
  	});
  }

  $('#ajax_restore_password').submit(function() {
  	let form = $(this);

  	clear(form);
  	$('#ajax_restore_password').find('.message').text('');

  	let email = $(this).find('input[type="email"]').first();

  	field_validation(email, new RegExp(/^[A-Z0-9\._-]{2,30}@[A-Z0-9\._-]{2,10}\.[A-Z]{2,4}$/i));

  	let errors = $(this).find('.input.wrong');

  	if(errors.length > 0) return false;

    let form_data = $(this).serializeArray();

    form_data.push({name: 'action', value: 'ajax_restore_password'});

    $.ajax({
      type: 'post',
      url: theme.ajaxurl,
      dataType: 'json',
      data: form_data,
      beforeSend: function () {

      },
      success: function (response) {
				if (response.state == true) {
					$('#ajax_restore_password').find('.message').text('Link to password recovery was sent to email');
        } else {
        	alert(response.message);
        }

      },
      error: function (response) {
        console.log(response);
      }
    });

  	return false;
  });

});
