jQuery(document).ready(function($) {

    $(document).on('click', '.ajax_read_notifications', function() {

        let form_data = [];

        form_data.push({name: 'nonce', value: $(this).data('nonce')});

        $('.notification-item').each(function() {
            let attr = $(this).data('id');
            if(typeof attr === 'undefined' || attr === false) return true;
            form_data.push({name: 'id[]', value: attr});
        });

        form_data.push({name: 'action', value: 'ajax_read_notifications'});

        $.ajax({
            type: 'post',
            url: theme.ajaxurl,
            dataType: 'json',
            data: form_data,
            beforeSend: function () {

            },
            success: function (response) {
                if (response.state == true) {
                    $('.notification-item').each(function() {
                        let attr = $(this).data('id');
                        if (typeof attr === 'undefined' || attr === false) return true;
                        $('.notification-item').removeClass('unreaded');
                        $('.notification-item').addClass('readed');
                        $('.notifications_toggler').removeClass('active');
                        $('.mark_as_read').remove();
                    });
                } else {
                    alert(response.message);
                }

            },
            error: function (response) {
                console.log(response);
            }
        });

        return false;
    });

});
