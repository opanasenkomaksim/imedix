jQuery(document).ready(function($) {

	$(document).on("click", function (event) {
	  // If the target is not the container or a child of the container, then process
	  // the click event for outside of the container.
	  if ($(event.target).closest("#users_menu").length === 0) {
			$('#users_menu').collapse('hide');
	  }
	});

	$(document).on("click", function (event) {
	  // If the target is not the container or a child of the container, then process
	  // the click event for outside of the container.
	  if ($(event.target).closest("#notifications_menu").length === 0) {
			$('#notifications_menu').collapse('hide');
	  }
	});

	$(document).on("click", function (event) {
	  // If the target is not the container or a child of the container, then process
	  // the click event for outside of the container.
	  if ($(event.target).closest('.question_menu').length === 0) {
			$('.question_menu').collapse('hide');
	  }
	});

});
