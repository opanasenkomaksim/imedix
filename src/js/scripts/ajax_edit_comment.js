jQuery(document).ready(function($) {

	$('.edit_comment_link').click(function() {

  	let link = $(this);
    let previous_comment = link.closest('.comment-body');


    let form = $('.comment-respond').first();
    form.find('.comment-form').addClass('edit');
    form.find('#comment').text(previous_comment.find('.comment-content p').first().text());
    form.find('#comment_post_ID').first().after('<input type="hidden" name="comment_ID" value="' + $(this).attr('href') + '">');
    form.find('#comment_parent').remove();
    form.find('#comment_post_ID').remove();
    link.closest('.comment-body').replaceWith(form);

    return false;

  });

  $(document).on('click', '.comment-form.edit .send_btn', function() {

    let link = $(this);
    let form_data = $(this).closest('.comment-form').serializeArray();

    form_data.push({name: 'action', value: 'ajax_edit_comment'});

    $.ajax({
      type: 'post',
      url: theme.ajaxurl,
      dataType: 'json',
      data: form_data,
      beforeSend: function () {

      },
      success: function (response) {
				if (response.state == true) {
					link.closest('.comment-respond').replaceWith(response.comment);
        } else {
        	alert(response.message);
        }

      },
      error: function (response) {
        console.log(response);
      }
    });

		return false;
	});


});
