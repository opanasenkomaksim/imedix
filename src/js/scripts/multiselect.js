jQuery(document).ready(function($) {

  var methods = {
    init : function() {
      let multiselect = $(this);
      let display = multiselect.find('.display').first();
      let list = multiselect.find('ul').first();
      let items = multiselect.find('li');
      let search = multiselect.find('.search').first();

    	methods.change_search_width(multiselect);
      $(window).resize(function() {
      	methods.change_search_width(multiselect);
      });

      items.find('input').change(function(){
        let parent = $(this).closest('li');
        let value = $(this).val();
        let text = parent.find('span').first().text();

        if($(this).is(':checked')){
          methods.add_item(multiselect, value, text);
        }else{
          methods.remove_item(multiselect, value);
        }
        methods.change_search_width(multiselect);
      });

      multiselect.on('click', '.remove', function() {
      	let item = $(this).closest('.item');
      	let value = item.attr('data-value');

        methods.remove_item(multiselect, value);
      	methods.change_search_width(multiselect);
      });

      search.on('input', function() {
      	let multiselect = $(this).closest('.multiselect');
      	let items = multiselect.find('ul input:not(:checked)');
      	let text = $(this).val().toLowerCase();

      	if(text !== '') {
      		multiselect.find('ul').removeClass('disabled');
      	} else {
      		multiselect.find('ul').addClass('disabled');
      	}

      	items.each(function() {
      		if ($(this).siblings('span').text().toLowerCase().indexOf(text) === 0) {
      			$(this).closest('li').removeClass('disabled');
      		} else {
      			$(this).closest('li').addClass('disabled');
      		}
      	});

      	methods.change_search_width(multiselect);
      });

      $(document).on('click', function(event) {
			  if ($(event.target).closest('.multiselect').length === 0) {
					$('.multiselect').find('ul').addClass('disabled');
			  }
      });

      // list.hide();
    },
    add_item : function(object, value, text) {
      let display = object.find('.display .span_wrapper').first();
      let item = object.find('ul input[value="' + value + '"]').first();

      object.find('.search').val('');
    	object.find('.search').first().css({'width': '50px'});
      display.append('<span data-value="' + value+'" class="item">' + text + '<a class="remove" href="javascript:;"><i class="far fa-times"></i></a></span>');

      object.find('ul').addClass('disabled');
      object.find('li').addClass('disabled');

      // methods.show_checked_items(object)
    },
    remove_item : function(object, value) {
      let item = object.find('ul input[value="' + value + '"]').first();

      object.find('.search').val('');
      object.find('.search').first().css({'width': '50px'});
      object.find('.display .span_wrapper span[data-value="' + value + '"]').remove();

      item.prop('checked', false);
      item.closest('li').removeClass('disabled');

      // methods.show_checked_items(object);
    },
    show_checked_items : function(object) {
    	let items = object.find('ul input:checked');
    	items.each(function() {
    		alert($(this).val());
    	});
    },
    change_search_width : function(object) {
			let length = 0;
			object.find('.item').each(function() {
				length = length + $(this).outerWidth(true);
			});
			let display = object.find('.display').first();
			let search = object.find('.search').first();
			let width = display.width();
			if(length / width <= 1) {
				width = width - length;
			} else {
				width = length % width;
			}
			search.css({'width': '50px'});
			search.width(Math.max(width - search.css('padding-left').replace("px", "") - search.css('padding-right').replace("px", ""), search.width()));
    }

  };

  $.fn.multiselect = function( method ) {

    return this.each(function(){
      // логика вызова метода
      if ( methods[ method ] ) {
        return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
      }else if( typeof method === 'object' || ! method ) {
        return methods.init.apply( this, arguments );
      }else{
        $.error( 'Метод с именем ' +  method + ' не существует для jQuery.multiselect' );
      }
    });

  }

});
