<?php get_header(); ?>
<main>
    <div class="container">

        <ol class="list-unstyled" id="breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
            <li class="item" itemprop="itemListElement" itemscope
                itemtype="http://schema.org/ListItem">
                <a itemprop="item" href="<?php echo home_url(); ?>">
                    <span itemprop="name">HOME</span></a>
                <meta itemprop="position" content="1"/>
            </li>
            <li class="item" itemprop="itemListElement" itemscope
                itemtype="http://schema.org/ListItem">
                <a itemprop="item" href="<?php the_permalink(); ?>">
                    <span itemprop="name"><?php the_title(); ?></span></a>
                <meta itemprop="position" content="2"/>
            </li>
        </ol>

        <div class="row">
            <div class="col-lg-9">

                <?php if (have_posts()) : ?>
                    <?php while (have_posts()) : the_post(); ?>
						<?php 
							$tags = wp_get_post_tags( get_the_ID() );
						?>
                        <article id="article-any">

                            <div>
                                <h1 id="article_hedline"><?php the_title(); ?></h1>
								<?php
									if(is_category_post( 'blog' )) {
										include get_template_directory() . '/templates/article_tags.php';
									}
								?>
                                <div class="row align-items-center">
                                    <div class="col"></div>
                                    <div class="col-12 text-col">
                                        <?php the_content(); ?>
                                    </div>
                                </div>
								<?php
									if(is_category_post( 'blog' )) {
										include get_template_directory() . '/templates/info_block.php'; ?>
										<br />
										<ul class="list-unstyled tags-list">
											<?php
											$categories = wp_get_post_tags( get_the_ID(), array('fields' => 'all') );
											?>
											<?php foreach ($categories as $category) : ?>
												<li class="item">

													<div class="open_favorites_menu left add">
														<a href="<?php echo get_category_link( $category->term_id ); ?>" class="item_link white">
															<?php echo $category->name ?>
														</a>
														<?php display_favorites_menu( 'category', $category->term_id ); ?>
													</div>

												</li>
											<?php endforeach; ?>
										</ul>
									<?php }
								?>
                            </div>

                        </article>

                    <?php endwhile;
                    wp_reset_query(); ?>
                <?php endif; ?>

            </div>
            <aside class="col-lg-3 sidebar-col d-none d-lg-block">
                <?php
                if (!dynamic_sidebar('post_sidebar')) _e('Add widgets to sidebar', 'imedix');
                ?>
            </aside>
        </div>
    </div>
</main>

<?php get_footer(); ?>
