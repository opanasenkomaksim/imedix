<article class="post-id-<?php echo the_ID(); ?>">

	<?php include get_template_directory() . '/templates/question_menu.php'; ?>

	<div class="row content-row">
		<?php if(has_post_thumbnail()) : ?>
		<div class="col-12 col-sm-4 order-sm-3 thumbnail-col">
			<?php the_post_thumbnail( 'medium', array( 'class' => 'article_image' ) ); ?>
		</div>
		<?php endif; ?>
		<div class="col-12">

			<?php include get_template_directory() . '/templates/question_tags.php'; ?>

		</div>
		<div class="col-12 title-col">
			<h3 class="title">
				<?php the_title(); ?>
			</h3>
			<div class="info-block">
				<div class="user_thumbnail">
					<?php echo get_avatar( get_the_author_meta( 'user_email' ), '40' ); ?>
				</div>
				<div class="data">
					<div class="author">
						<div class="name">
							<?php the_author(); ?>
						</div>
						<?php
							$author_id = get_the_author_meta('ID');
							$author_position = get_field( 'position', 'user_'. $author_id );
							if ( $author_position ) :
						?>
						<div class="position">
							<?php echo $author_position; ?>
						</div>
						<?php endif; ?>
					</div>
					<div class="date">
						<?php
							_e('Published at', 'imedix');
							printf( ' %1$s', get_the_date( 'd.m.Y H:i' ) );
						?>
					</div>
				</div>
			</div>
		</div>
		<div class="col-12 col-sm-8 order-sm-4 text-col">
			<div class="info-block">
				<div class="user_thumbnail">
					<?php echo get_avatar( get_the_author_meta( 'user_email' ), '40' ); ?>
				</div>
				<div class="data">
					<div class="author">
						<div class="name">
							<?php the_author(); ?>
						</div>
						<?php
							$author_id = get_the_author_meta('ID');
							$author_position = get_field('position', 'user_'. $author_id );
							if ( $author_position ) :
						?>
						<div class="position">
							<?php echo $author_position; ?>
						</div>
						<?php endif; ?>
					</div>
					<div class="date">
						<?php
							_e('Published at', 'imedix');
							printf( ' %1$s', get_the_date( 'd.m.Y H:i' ) );
						?>
					</div>
				</div>
			</div>
			<div class="text">
				<?php echo_excerpt( 400, '' ); ?>
			</div>
		</div>
	</div>
	<div class="row align-items-center bottom-row">
		<div class="col">
			<div class="row">
				<div class="col-auto item">
					<img src="<?php echo get_template_directory_uri(); ?>/production/images/comments_icon.png"
						 srcset="<?php echo get_template_directory_uri(); ?>/production/images/comments_icon@2x.png 2x,
								 <?php echo get_template_directory_uri(); ?>/production/images/comments_icon@3x.png 3x"
						 class="comments_image">
					<span class="text">
						<span class="description">Respond: </span><?php echo get_comments_number(); ?>
					</span>
				</div>
				<div class="col-auto item">
					<img src="<?php echo get_template_directory_uri(); ?>/production/images/views_icon.png"
						 srcset="<?php echo get_template_directory_uri(); ?>/production/images/views_icon@2x.png 2x,
								 <?php echo get_template_directory_uri(); ?>/production/images/views_icon@3x.png 3x"
						 class="comments_image">
					<span class="text">
						<span class="description">Views: </span><?php display_views(); ?>
					</span>
				</div>
			</div>
		</div>
		<div class="col-auto">
			<a href="<?php the_permalink(); ?>" class="more_btn">
				View more
			</a>
		</div>
	</div>
</article>
