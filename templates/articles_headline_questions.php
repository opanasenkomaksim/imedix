<div id="articles_headline">
	<div class="row">
		<div class="col-md-6 title-col">
			<h2 class="title">
				Category: <?php single_cat_title(); ?>
			</h2>
			<?php if ( $category_description = category_description() ) : ?>
			<div class="description">
				<?php echo $category_description; ?>
			</div>
			<?php endif; ?>
			<a href="#" class="gray_btn">
				<i class="fal fa-plus"></i> Add Favorites
			</a>
		</div>
		<div class="col-md-6">
			<?php if( is_user_logged_in() ) : ?>
			<div class="row justify-content-end">
				<div class="col-auto">
					<a class="orange_btn question_editor_popup_link">
						<i class="fal fa-plus"></i> add Question
					</a>
				</div>
			</div>
			<?php endif; ?>
			<?php
				//$filters = array( 'Interesting', 'Top Week', 'Top Month', 'All' );
				$filters = array( 'Interesting', 'All' );
				$filters_slugs = array_map( 'strtolower', $filters );
				$filters_slugs = str_replace( ' ', '-', $filters_slugs );
				$filter = ! empty( $_GET[ 'filter' ] ) && in_array( $_GET[ 'filter' ], $filters_slugs ) ? $_GET[ 'filter' ] : 'all';
			?>
			<!--<div class="row">
				<div class="col">
					<ul class="list-unstyled panel-list">
						<?php foreach ($filters as $key => $value) : ?>
						<li class="item">
							<a href="<?php echo add_query_arg( array( 'filter' => $filters_slugs[ $key ] ) ); ?>" class="item_link<?php echo $filter == $filters_slugs[ $key ] ? ' active' : ''; ?>">
								<?php echo $value; ?>
							</a>
						</li>
						<?php endforeach; ?>
					</ul>
				</div>
			</div>-->
		</div>
	</div>
</div>
