<div id="registration_popup" class="fancybox-content" style="display: none;">
	<div class="title">
		Sign Up
	</div>
	<form action="" class="ajax_registration" novalidate>
		<div class="row">
			<div class="col-6">
				<a href="<?php echo home_url('/wp-login.php?loginSocial=google'); ?>" data-plugin="nsl" data-action="connect" data-redirect="current" data-provider="google" data-popupwidth="600" data-popupheight="600" class="google_btn">
					<img src="<?php echo get_template_directory_uri(); ?>/production/images/logo-google.png"
srcset="<?php echo get_template_directory_uri(); ?>/production/images/logo-google@2x.png 2x,
       <?php echo get_template_directory_uri(); ?>/production/images/logo-google@3x.png 3x"
class="google_logo"> Google
				</a>
			</div>
			<div class="col-6">
				<a href="<?php echo home_url('/wp-login.php?loginSocial=facebook'); ?>" data-plugin="nsl" data-action="connect" data-redirect="current" data-provider="facebook" data-popupwidth="475" data-popupheight="175" class="facebook_btn">
					<i class="fab fa-facebook-square"></i> Facebook
				</a>
			</div>
			<div class="col-12 or-col">
				or
			</div>
			<div class="col-6 input-col">
				<label>
					<i>*</i> <?php _e('First Name', 'imedix'); ?>
				</label>
				<input type="text" name="name" class="name" placeholder="John" data-empty="Name is Required" data-wrong="Wrong Name">
			</div>
			<div class="col-6 input-col">
				<label>
					<i>*</i> <?php _e('Last Name', 'imedix'); ?>
				</label>
				<input type="text" name="lastname" class="lastname" placeholder="Johnson" data-empty="Lastname is Required" data-wrong="Wrong Lastname">
			</div>
			<div class="col-12 input-col">
				<label>
					<i>*</i> <?php _e('Email', 'imedix'); ?>
				</label>
				<input type="email" name="email" class="email" data-empty="Email is Required" data-wrong="Wrong Email">
			</div>
			<div class="col-12 input-col last">
				<label>
					<i>*</i> <?php _e('Password', 'imedix'); ?>
				</label>
				<input type="password" name="password" class="password" data-empty="Password is Required" data-wrong="Minimum six characters needed">
			</div>
			<div class="col-12 info">
				By clicking "Sign Up", you acknowledge that you have read our updated terms of service, privacy policy and cookie policy, and that your continued use of the website is subject to these policies.
			</div>
			<div class="col-12 text-center">
				<?php stl_wp_nonce_field('ajax-registration', 'security'); ?>
				<input type="submit" class="blue_btn" value="Sign Up">
			</div>
			<div class="col-12 text-center link-col">
				<a href="#" class="link login_popup_link">Already have accaunt? Login.</a>
			</div>
		</div>
	</form>
</div>
