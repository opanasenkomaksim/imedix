<div id="confirm_email_popup" class="fancybox-content" style="display: none;">
	<div class="title">
		Confirm Email
	</div>
	<div class="row">
		<div class="col-12 text-center">
			<div class="text">
				A letter was sent to your email containing a link to confirm the email.
			</div>
			<a href="#" class="email_confirmation_link send_again_btn">
				Send Confirmation Again
			</a>
		</div>
	</div>
</div>
