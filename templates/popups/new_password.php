<div id="new_password_popup" class="fancybox-content" style="display: none;">
	<div class="title">
		<?php if ( is_user_logged_in() ) : ?>
		Change Password
		<?php else : ?>
		Restore Password
		<?php endif; ?>
	</div>
	<form action="" id="ajax_new_password" novalidate>
		<div class="row">
			<div class="col-12 input-col">
				<label>
					<i>*</i> <?php _e('Password', 'imedix'); ?>
				</label>
				<input type="password" name="password" class="password">
			</div>
			<div class="col-12 input-col last">
				<label>
					<i>*</i> <?php _e('Password', 'imedix'); ?>
				</label>
				<input type="password" name="password" class="password" data-empty="Passwords are Required" data-wrong="Passwords are not the same">
			</div>
			<div class="col-12 text-center">
				<?php stl_wp_nonce_field('ajax-new-password', 'security'); ?>
				<input type="hidden" name="nonce" value="<?php echo ! empty( $_GET[ 'nonce' ] ) ? $_GET[ 'nonce' ] : ''; ?>">
				<input type="hidden" name="email" value="<?php echo ! empty( $_GET[ 'email' ] ) ? $_GET[ 'email' ] : ''; ?>">
				<input type="hidden" name="key" value="<?php echo ! empty( $_GET[ 'key' ] ) ? $_GET[ 'key' ] : ''; ?>">
				<input type="submit" class="new_password_btn" value="<?php echo is_user_logged_in() ? 'Change Password': 'Restore Password'; ?>">
				<div class="message"></div>
			</div>
		</div>
	</form>
</div>
