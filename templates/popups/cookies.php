<div id="cookies_popup" class="fancybox-content" style="display: none;">
    <div class="text">
        Imedix uses cookies to improve your experience, help personalize content and ads, and provide a safer
        experience. By continuing, you agree to this use. To learn more, see our <a
                href="<?php echo home_url('/privacy'); ?>">Cookie Policy</a>.
    </div>
</div>
