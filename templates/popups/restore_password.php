<div id="restore_password_popup" class="fancybox-content" style="display: none;">
	<div class="title">
		Restore Password
	</div>
	<form action="" id="ajax_restore_password" novalidate>
		<div class="row">
			<div class="col-12 input-col last">
				<label>
					<i>*</i> <?php _e('Email', 'imedix'); ?>
				</label>
				<input type="email" name="email" class="email" data-empty="Email is Required" data-wrong="Wrong Email">
			</div>
			<div class="col-12 text-center">
				<?php stl_wp_nonce_field('ajax-restore-password', 'security'); ?>
				<input type="submit" class="restore_password_btn" value="Restore Password">
				<div class="message"></div>
			</div>
			<div class="col-12 text-center link-col">
				<a href="#" class="link login_popup_link">Remeber Password? Please Login.</a>
			</div>
			<div class="col-12 text-center link-col">
				<a href="#" class="link registration_popup_link">Don’t have accaunt? Please Sign Up.</a>
			</div>
		</div>
	</form>
</div>
