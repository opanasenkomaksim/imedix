<div id="question_editor_popup" class="fancybox-content" style="display: none;">
	<form action="" id="ajax_question_editor">
		<div class="row">
			<div class="col-12">
				<div class="title">
					Add question
				</div>
			</div>
			<div class="col-12 input-col">
				<label class="label">Title</label>
				<input type="text" name="title" class="title_input" placeholder="Write Your Title Here">
			</div>
			<div class="col-12 input-col">
				<label class="label">Category</label>
        <div class="multiselect">
          <div class="display">
          	<span class="span_wrapper"></span>
        		<input type="text" class="search">
	        </div>
          <ul class="disabled">
          	<?php
          		$term = get_term_by('slug', 'qa', 'category');
          		$child_terms = get_term_children( $term->term_id, 'category' );
          	?>
          	<?php foreach( $child_terms as $term_id ) : ?>
          	<?php $term = get_term_by( 'id', $term_id, 'category' ); ?>
            <li class="disabled"><label><input type="checkbox" name="category[]" class="category_input" value="<?php echo $term->term_id; ?>"><span><?php echo $term->name; ?></span></label></li>
	          <?php endforeach; ?>
          </ul>
        </div>
        <div class="description">
        	You Can Insert Maximum 5 Categoryes
        </div>

			</div>
			<div class="col-12 input-col">
				<label class="label">Detail</label>
				<textarea id="question_textarea" name="question_textarea"></textarea>
			</div>
			<div class="col-12 button-col">
				<span class="description">
					Will Be Post After Moderation
				</span>
				<input type="hidden" name="post_id" value="0">
				<input type="hidden" name="is_single" value="<?php echo is_single(); ?>">
				<?php stl_wp_nonce_field('ajax-question-editor', 'security'); ?>
				<button class="blue_btn">
					Publish
				</button>
			</div>
		</div>
	</form>
</div>

<div id="question_editor_success_popup" class="fancybox-content" style="display: none;">
	<div class="row m-0 align-items-center">
		<div class="col-auto mark-col">
			<i class="fas fa-check-circle"></i>
		</div>
		<div class="col text-col">
			Question successfully added
		</div>
	</div>
</div>
