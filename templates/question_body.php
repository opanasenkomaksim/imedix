<div id="article_body">

	<?php include get_template_directory() . '/templates/article_tags.php'; ?>

	<h1 id="article_hedline" class="question"><span itemprop="name">
		<?php the_title(); ?>
	</h1>

	<div class="row align-items-center">
		<div class="col-12 text-col">
			<?php if(has_post_thumbnail()) : ?>
			<div class="article_thumbnail">
				<?php the_post_thumbnail( 'medium', array( 'itemprop' => 'image' ) ); ?>
			</div>
			<?php endif; ?>

			<div itemprop="text">
                <?php the_content(); ?>
            </div>

			<div class="col-sm info_block-col">

				<?php
					$user_id = get_the_author_meta('ID');
					$avatar_size = 40;
					$datetime = get_the_date('Y-m-d\TH:i');;
					$published_date = get_the_date( 'd.m.Y H:i' );
					include get_template_directory() . '/templates/info_block.php';
				?>

			</div>
		</div>

	</div>

</div>
