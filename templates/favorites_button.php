<?php
	// Needs
	// $ID
	// $add_favorites
	// $link_class
	// $data_nonce
?>

<a href="<?php echo $ID; ?>" class="gray_btn favorites_button <?php echo $link_class; ?>" data-nonce="<?php echo $data_nonce; ?>">
	<?php if ( $add_favorites ) : ?>
	<i class="far fa-plus"></i> Add Favorites
	<?php else : ?>
	<i class="far fa-minus"></i> Remove from favorites
	<?php endif; ?>
</a>
