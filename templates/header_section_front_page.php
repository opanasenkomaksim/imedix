<section id="header-section">
	<div class="circle_1"></div>
	<div class="circle_2"></div>
	<div class="circle_3"></div>
	<div class="circle_4"></div>
	<div class="circle_5"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<h1 class="section_title">
					<?php _e('What is iMedix?', 'imedix'); ?>
				</h1>
				<div class="text">
					<p>
						iMedix is a free website that helps you find and share health information.
					</p>
					<p>
						Members of the iMedix community share their experiences and rank medical content in order to make health information personal, organized and accessible to everyone.
					</p>
				</div>
			</div>
			<div class="col-lg-6">
				<form action="" class="ajax_registration" novalidate>
					<div class="row">
						<div class="col-6">
							<a href="<?php echo home_url('/wp-login.php?loginSocial=google'); ?>" data-plugin="nsl" data-action="connect" data-redirect="current" data-provider="google" data-popupwidth="600" data-popupheight="600" class="google_btn">
								<img src="<?php echo get_template_directory_uri(); ?>/production/images/logo-google.png"
     srcset="<?php echo get_template_directory_uri(); ?>/production/images/logo-google@2x.png 2x,
             <?php echo get_template_directory_uri(); ?>/production/images/logo-google@3x.png 3x"
     class="google_logo"> Google
							</a>
						</div>
						<div class="col-6">
							<a href="<?php echo home_url('/wp-login.php?loginSocial=facebook'); ?>" data-plugin="nsl" data-action="connect" data-redirect="current" data-provider="facebook" data-popupwidth="475" data-popupheight="175" class="facebook_btn">
								<i class="fab fa-facebook-square"></i> Facebook
							</a>
						</div>
						<div class="col-12 or-col">
							or
						</div>
						<div class="col-6 input-col">
							<label>
								<i>*</i> <?php _e('First Name', 'imedix'); ?>
							</label>
							<input type="text" name="name" class="name" placeholder="John" data-empty="Name is Required" data-wrong="Wrong Name">
						</div>
						<div class="col-6 input-col">
							<label>
								<i>*</i> <?php _e('Last Name', 'imedix'); ?>
							</label>
							<input type="text" name="lastname" class="lastname" placeholder="Johnson" data-empty="Lastname is Required" data-wrong="Wrong Lastname">
						</div>
						<div class="col-12 input-col">
							<label>
								<i>*</i> <?php _e('Email', 'imedix'); ?>
							</label>
							<input type="email" name="email" class="email" data-empty="Email is Required" data-wrong="Wrong Email">
						</div>
						<div class="col-12 input-col last">
							<label>
								<i>*</i> <?php _e('Password', 'imedix'); ?>
							</label>
							<input type="password" name="password" class="password" data-empty="Password is Required" data-wrong="Minimum six characters needed">
						</div>
						<div class="col-12 info">
							By clicking "Sign Up", you acknowledge that you have read our updated terms of service, privacy policy and cookie policy, and that your continued use of the website is subject to these policies.
						</div>
						<div class="col-12 text-center">
							<?php stl_wp_nonce_field('ajax-registration', 'security'); ?>
							<input type="submit" class="transparent_btn" value="Sign Up">
						</div>
					</div>
				</form>
				<a class="transparent_btn popup_link registration_popup_link">
					Sign Up
				</a>
			</div>
		</div>
	</div>
</section>
