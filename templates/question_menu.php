<?php if( get_current_user_id() == get_the_author_meta('ID') ) : ?>
	<?php $id = uniqid('qm_'); ?>
	<a href="#<?php echo $id; ?>" data-toggle="collapse" class="question_menu_link"><i class="fas fa-ellipsis-v"></i></a>
	<ul id="<?php echo $id; ?>" class="list-unstyled collapse question_menu">
		<li class="item">
			<a href="<?php the_ID(); ?>" class="item_link edit_question_link" data-nonce="<?php echo wp_create_nonce('ajax_get_question'); ?>">Edit</a>
		</li>
		<li class="item">
			<a href="<?php the_ID(); ?>" class="item_link delete_question_link" data-nonce="<?php echo wp_create_nonce('ajax_remove_question'); ?>" data-is_single="<?php echo is_single(); ?>" data-redirect="<?php echo home_url(); ?>">Delete</a>
		</li>
	</ul>
<?php endif; ?>
