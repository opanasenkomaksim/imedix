<article class="article-user">

	<div class="top">
		<?php
			$position = ( $paged - 1 ) * $options['number'] + $i;
		?>
		Position: <?php echo $position; ?>
	</div>
	<div class="info">
		<a class="user_thumbnail" href="<?php echo get_author_posts_url($user->ID); ?>">
			<?php echo get_avatar( $user->user_email, '40' ); ?>
		</a>
		<div class="data">
			<div class="author" itemprop="author" itemscope itemtype="http://schema.org/Person">
				<a class="name" href="<?php echo get_author_posts_url($user->ID); ?>" itemprop="name">
					<?php echo $user->display_name; ?>
				</a>
				<?php
					$user_position = get_field( 'position', 'user_'. $user->ID );
				?>
				<?php if ( $user_position ) : ?>
				<div class="position">
					<?php echo $user_position; ?>
				</div>
				<?php endif; ?>
			</div>
			<?php
				$score = (int) get_user_meta( $user->ID, 'user_score', true );
				$followers_count = (int) get_user_meta( $user->ID, 'followers_count', true );
			?>
			<div class="information">
				<span>
					Score: <span><?php echo $score; ?></span>
				</span>
				<span>
					Followers: <span><?php echo $followers_count; ?></span>
				</span>
			</div>
		</div>
	</div>

</article>
