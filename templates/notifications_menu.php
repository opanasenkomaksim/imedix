<div id="notifications_menu" class="collapse">
	<ul class="list-unstyled events-list">
        <?php if ( $unreaded_count > 0 ) : ?>
        <li class="item mark_as_read" >
            <div class="top_item">
                <a href="#" class="ajax_read_notifications" data-nonce="<?php echo wp_create_nonce('ajax_read_notifications'); ?>">
                    Mark all as read
                </a>
            </div>
        </li>
        <?php endif; ?>
		<?php foreach ( $notifications->notifications as $notification ) : ?>
		<?php
			$action = $notification->action;
			$user_id = $notification->user_action_id;
			$user = get_user_by( 'ID', $user_id );
			switch ( $action ) {
				case 'answer':
					$post_id = $notification->post_action_id;
					$post_title = get_post_field( 'post_title', $post_id );
					$comment_id = $notification->comment_action_id;
					$href = get_post_field( 'guid', $post_id ) . '#comment-' . $comment_id;
					$text = 'answered in your topic <span>“' . $post_title . '”</span>';
					break;

				case 'comment':
					$post_id = $notification->post_action_id;
					$post_title = get_post_field( 'post_title', $post_id );
					$comment_id = $notification->comment_action_id;
					$href = get_post_field( 'guid', $post_id ) . '#comment-' . $comment_id;
					$text = 'commented your answer in topic <span>“' . $post_title . '”</span>';
					break;

				case 'upvote':
					$post_id = $notification->post_action_id;
					$post_title = get_post_field( 'post_title', $post_id );
					$comment_id = $notification->comment_action_id;
					$comment = get_comment( $comment_id );
					$word = $comment->comment_parent == 0 ? 'answer' : 'comment';
					$href = get_post_field( 'guid', $post_id ) . '#comment-' . $comment_id;
					$text = 'upvoted your ' . $word . ' in topic <span>“' . $post_title . '”</span>';
					break;

				case 'follower':
					$text = 'follow you';
					$href = get_author_posts_url($user_id);
					break;

				default:
					continue;
					break;
			}
		?>
		<li class="item notification-item <?php echo $notification->readed == 1 ? 'readed' : 'unreaded'; ?>" data-id="<?php echo $notification->ID; ?>">
			<a href="<?php echo $href; ?>" class="item_link">
				<?php echo $user->display_name . ' ' . $text; ?>
			</a>
		</li>
		<?php endforeach; ?>
		<?php if ( $count == 0 ) : ?>
			<li class="item" >
				<span class="item_text">
                    You haven't got notifications
                </span>
			</li>
		<?php endif; ?>
        <?php if ( $display_count < $count ) : ?>
            <li class="item" >
                <div class="bottom_item">
                    <a href="<?php echo home_url('/notifications/'); ?>" class="more">
                        See all
                    </a>
                </div>
            </li>
        <?php endif; ?>
	</ul>
</div>
