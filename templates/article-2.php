<div class="col-md-4">

	<article class="post-id-<?php echo the_ID(); ?> article-2">

		<?php if(has_post_thumbnail()) : ?>
		<div class="thumbnail">
			<?php the_post_thumbnail( 'medium', array( 'class' => 'article_image' ) ); ?>
			<div class="tag">Review</div>
		</div>
		<?php endif; ?>
		<div class="text-center">
			<?php
				$verdict = get_field( 'verdict' );
				$temp = $verdict[ 'rating' ];
				$temp = is_numeric( $temp ) && $temp > 0 && $temp <= 5 ? $temp : 3;
			?>
			<div id="display_rating">
				<?php for( $i = 1; $i <= min( 5, $temp ); $i++ ) : ?>
				<span class="checked"></span>
				<?php endfor; ?>
				<?php for( $i = 0; $i < 5 - min( 5, $temp ); $i++ ) : ?>
				<span></span>
				<?php endfor; ?>
			</div>
		</div>
		<div class="title">
			<?php the_title(); ?>
		</div>
		<div class="text-center">
			<a href="<?php the_permalink(); ?>" class="blue_btn">
				View more
			</a>
		</div>

	</article>

</div>
