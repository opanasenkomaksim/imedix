<article class="article-404">

    <div class="content">
        <div class="text_404">
            404
        </div>
        <div class="description">
            PAGE NOT FOUND
        </div>
    </div>
    <div class="text">
        Perhaps the page was deleted, the URL is incorrect, or some other error occured.
    </div>

    <a href="<?php echo home_url(); ?>" class="orange_btn">
        GO TO MAIN PAGE
    </a>

</article>