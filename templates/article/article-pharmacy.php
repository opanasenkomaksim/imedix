<div class="col-md-4">

	<article class="post-id-<?php echo the_ID(); ?> article-pharmacy">

		<div class="thumbnail">
		    <?php if(has_post_thumbnail()) : ?>
			<?php the_post_thumbnail( 'pharmacy_reviews', array( 'class' => 'article_image' ) ); ?>
            <?php else : ?>
            <img src="<?php echo get_template_directory_uri(); ?>/production/images/pharmacy_reviews_thumbnail.png"
                 srcset="<?php echo get_template_directory_uri(); ?>/production/images/pharmacy_reviews_thumbnail@2x.png 2x,
                                             <?php echo get_template_directory_uri(); ?>/production/images/pharmacy_reviews_thumbnail@3x.png 3x"
                 class="article_image">
		    <?php endif; ?>
			<div class="tag">Review</div>
		</div>
		<!--<div class="text-center">
			<?php
				$verdict = get_field( 'verdict' );
				$temp = $verdict[ 'rating' ];
				$temp = is_numeric( $temp ) && $temp > 0 && $temp <= 5 ? $temp : 3;
			?>
            <?php display_rating($temp); ?>
		</div>-->
		<div class="title">
			<?php the_title(); ?>
		</div>
		<div class="text-center">
			<a href="<?php the_permalink(); ?>" class="blue_btn">
				View more
			</a>
		</div>

	</article>

</div>
