<?php
	// Needs
	// $ID
	// $add_favorites
	// $link_class
	// $data_nonce
?>
<a href="<?php echo $ID; ?>" class="favorites_small_button <?php echo $link_class; ?>" data-nonce="<?php echo $data_nonce; ?>">
	<div class="ico_wr">
		<?php if ( $add_favorites ) : ?>
		<i class="far fa-plus"></i>
		<?php else : ?>
		<i class="far fa-minus"></i>
		<?php endif; ?>
	</div>
</a>
