<?php
/**
 * Created by seattleby.com
 * Date: 23.05.2019
 * Time: 8:39
 */
?>

<span class="rating_inputs">
    <label>
        <input type="radio" name="rating" value="1"/>
        <span class="icon"></span>
        <span class="description">(1/5)</span>
    </label>
    <label>
        <input type="radio" name="rating" value="2"/>
        <span class="icon"></span>
        <span class="icon"></span>
        <span class="description">(2/5)</span>
    </label>
    <label>
        <input type="radio" name="rating" value="3"/>
        <span class="icon"></span>
        <span class="icon"></span>
        <span class="icon"></span>
        <span class="description">(3/5)</span>
    </label>
    <label>
        <input type="radio" name="rating" value="4"/>
        <span class="icon"></span>
        <span class="icon"></span>
        <span class="icon"></span>
        <span class="icon"></span>
        <span class="description">(4/5)</span>
    </label>
    <label>
        <input type="radio" name="rating" value="5" checked/>
        <span class="icon"></span>
        <span class="icon"></span>
        <span class="icon"></span>
        <span class="icon"></span>
        <span class="icon"></span>
        <span class="description">(5/5)</span>
    </label>
</span>