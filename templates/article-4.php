<article class="article-4">

	<div class="entry">
		<?php
			$comment_post = get_post( $comment->comment_post_ID );
		?>
		<?php echo stl_get_comment_depth( $comment->comment_ID ) == 1 ? 'Answered' : 'Commented'; ?> in the  topic <a href="<?php echo $comment_post->guid; ?>" class="link">“<?php echo $comment_post->post_title; ?>”</a>
	</div>
	<div>
		<?php
			$user_id = $user->ID;
			$avatar_size = 40;
			$datetime = sprintf( '%1$s\T%2$s', get_comment_date( 'Y-m-d', $comment ), get_comment_time( 'H:i' ) );
			$published_date = sprintf( '%1$s %2$s', get_comment_date( 'd.m.Y', $comment ), get_comment_time( 'H:i' ) );
			include get_template_directory() . '/templates/info_block.php';
		?>
	</div>
	<div class="comment_text">
		<?php echo $comment->comment_content; ?>
	</div>

</article>
