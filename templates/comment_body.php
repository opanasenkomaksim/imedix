<div class="col">
    <footer class="comment-meta">
        <?php
        $user_id = $comment->user_id;
        $avatar_size = 40;
        $datetime = get_comment_time('c');
        $published_date = sprintf(' %1$s %2$s', get_comment_date('d.m.Y', $comment), get_comment_time('H:i'));
        include get_template_directory() . '/templates/info_block.php';
        ?>
        <?php if ('0' == $comment->comment_approved) : ?>
            <p class="comment-awaiting-moderation"><?php _e('(Awaiting moderation)', 'imedix'); ?></p>
        <?php endif; ?>
    </footer><!-- .comment-meta -->

    <div class="comment-content">
        <?php comment_text(); ?>
    </div><!-- .comment-content -->

    <?php
    $user = wp_get_current_user();
    $allowed_roles = array('editor', 'administrator', 'author');
    ?>
		<?php if (is_category_post( 'qa' )): ?>
			<?php if (array_intersect($allowed_roles, $user->roles) && '0' != $comment->comment_approved && $depth === 1 && !$checked) : ?>
					<form action="" class="get_best_answer">
							<div class="row get_best_answer-row">
									<div class="col d-flex justify-content-end align-items-center">
											<div class="question">
													Do you find answer in your question?
											</div>
											<?php wp_nonce_field('ajax-best-comment', 'security'); ?>
											<input type="hidden" name="post_id" value="<?php the_ID(); ?>">
											<input type="hidden" name="comment_id" value="<?php comment_ID(); ?>">
											<button class="yes_btn" value="yes">
													YES
											</button>
											<button class="no_btn" value="no">
													NO
											</button>
									</div>
							</div>
					</form>
			<?php endif; ?>
    <?php endif; ?>

    <?php if ($depth < 2 && comments_open()) : ?>
        <?php
        $id = uniqid('cm_');
        ?>
        <div class="collapse_link_wrapper">
            <a href="#<?php echo $id; ?>" class="collapse_link" data-toggle="collapse">
                <span><?php _e('Reply', 'imedix'); ?></span>
                <img src="<?php echo get_template_directory_uri(); ?>/production/images/reply_icon.png"
                     srcset="<?php echo get_template_directory_uri(); ?>/production/images/reply_icon@2x.png 2x,
				             <?php echo get_template_directory_uri(); ?>/production/images/reply_icon@3x.png 3x"
                     class="reply_icon">
            </a>
        </div>
        <div class="collapse" id="<?php echo $id; ?>">
            <?php display_comment_form(get_comment_ID()); ?>
        </div>
    <?php endif; ?>

</div>
