<ul class="list-unstyled tags-list">
    <?php
    $slug = false;
    if ( is_needed_category( 'qa' ) || is_category_post( 'qa' ) ) {
        $slug = 'qa';
    } else if ( is_needed_category( 'drugs' ) || is_category_post( 'drugs' ) ) {
        $slug = 'drugs';
    } else if ( is_needed_category( 'pharmacy-reviews' ) || is_category_post( 'pharmacy-reviews' ) ) {
        $slug = 'pharmacy-reviews';
    }
    $term = ! empty( $slug ) ? get_term_by('slug', $slug, 'category') : false;
    ?>
    <?php if ( $term ) : ?>
        <li class="item">
            <a href="<?php echo get_category_link( $term->term_id ); ?>" class="item_link blue">
                <?php echo $term->name; ?>
            </a>
        </li>
    <?php endif; ?>
    <?php if( ! comments_open() || post_has_the_best_comment() ) : ?>
        <li class="item">
            <a href="#" class="item_link red">
                Closed
            </a>
        </li>
    <?php endif; ?>
    <?php
    $categories = $slug != 'pharmacy-reviews' ? wp_get_post_categories( get_the_ID(), array('fields' => 'all') ) : array();
    ?>
    <?php foreach ($categories as $category) : ?>
        <li class="item">

            <div class="open_favorites_menu left add">
                <a href="<?php echo get_category_link( $category->term_id ); ?>" class="item_link white">
                    <?php echo $category->name ?>
                </a>
                <?php display_favorites_menu( 'category', $category->term_id ); ?>
            </div>

        </li>
    <?php endforeach; ?>
    <li class="item article_status">
    <?php if( $post->post_status == 'pending' ) : ?>
        <a href="#" class="item_link red">
            Awaiting moderation
        </a>
    <?php endif; ?>
    </li>
</ul>
