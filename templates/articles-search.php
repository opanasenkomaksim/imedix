<div id="articles">

<?php if ( have_posts() ) : ?>

	<div class="row">

		<?php $i = 1; while ( have_posts() ) : the_post(); ?>
		<?php include get_template_directory() . '/templates/article-any.php'; ?>

		<?php if ( $i == 2 || $i == 5 ) : ?>
			<div class="col-12 widgets-col">
				<?php
					$sidebar = 'search_page_content';
					if ( empty( $sidebar ) || ! dynamic_sidebar( $sidebar ) )
						_e('Add widgets to sidebar', 'imedix');
				?>
			</div>
		<?php endif; ?>

		<?php $i++; endwhile; ?>

	</div>

<?php else : ?>

	<?php include get_template_directory() . '/templates/article-empty.php'; ?>

<?php endif; ?>

</div>

<?php
  $paginate = paginate_links(array(
			// 'total' => 8,
			// 'current' => 1,
      // 'total' => $total,
      // 'current' => $paged,
      // 'base' => URI . '/my-account/manage-tours/' . '%_%',
      // 'format' => '%#%',
      'type' => 'array',
      'end_size' => 1,
      'mid_size' => 1,
      'prev_text' => '<i class="fas fa-caret-left"></i>',
      'next_text' => '<i class="fas fa-caret-right"></i>',
  ));
  // die(var_dump($paginate));
	display_pagination( $paginate );
?>
