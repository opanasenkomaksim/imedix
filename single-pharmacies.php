<?php get_header(); ?>

<main>
    <div class="container">

        <ol class="list-unstyled" id="breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
            <li class="item" itemprop="itemListElement" itemscope
                itemtype="http://schema.org/ListItem">
                <a itemprop="item" href="<?php echo home_url(); ?>">
                    <span itemprop="name">HOME</span></a>
                <meta itemprop="position" content="1"/>
            </li>
            <li class="item" itemprop="itemListElement" itemscope
                itemtype="http://schema.org/ListItem">
                <a itemprop="item" href="<?php echo home_url('/category/pharmacy-reviews/'); ?>">
                    <span itemprop="name">PHARMACY REVIEW</span></a>
                <meta itemprop="position" content="2"/>
            </li>
        </ol>

        <div class="row">
            <div class="col-lg-9">

                <?php if(have_posts()) : ?>
                    <?php while (have_posts()) : the_post(); ?>

                        <article id="article-any" itemscope itemtype="http://schema.org/Article">

                            <div itemid="<?php the_permalink() ?>" itemprop="mainEntityOfPage">
                           <!-- <div itemprop="itemReviewed" itemscope itemtype="http://schema.org/WebSite">-->
                                <h1 id="article_hedline" itemprop="headline"><?php the_title(); ?></h1>
																<meta itemprop="inLanguage" content="en-US">
																<meta itemprop="url" content="<?php the_permalink(); ?>">

                                <?php $thumbnail_url = get_the_post_thumbnail_url(); ?>
                                <?php if($thumbnail_url) : ?>
                                    <meta itemprop="image" content="<?php echo $thumbnail_url; ?>">
                                <?php endif; ?>

                                <?php $author = get_the_author_meta('first_name') . ' ' . get_the_author_meta('last_name'); ?>
                                <?php $author = $author == ' ' ? get_the_author() : $author; ?>
																<div style="display: none;" itemprop="author" itemscope itemtype="http://schema.org/Person">
																	<span itemprop="name"><?php echo $author; ?></span>
																</div>

                            </div>
														
														
														<div style="display: none;" itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
															<div itemprop="logo image" itemscope itemtype="https://schema.org/ImageObject">
																<img itemprop="url contentUrl" src="<?php echo esc_url(get_template_directory_uri()); ?>/production/images/logo.png" alt="logo" />
															</div>
															<meta itemprop="name" content="<?php echo $author; ?>" />
														</div>

														<div style="display: none;" itemprop="author" itemscope itemtype="http://schema.org/Person">
															<span itemprop="name"><?php echo $author; ?></span>
														</div>

                            <?php $verdict = get_field('verdict'); ?>
                            <div class="row align-items-center">
                                <div class="col-sm">
                                    <meta itemprop="datePublished" content="<?php the_date('Y-m-d\TH:i'); ?>">
                                    <meta itemprop="dateModified" content="<?php the_modified_time('Y-m-d')?>" />
                                    <?php
                                    $temp = $verdict['rating'];
                                    $temp = is_numeric($temp) && $temp > 0 && $temp <= 5 ? $temp : 3;
                                    ?>
                                    <!--<?php /* display_rating($temp); */ ?>
                                    <div class="rating_info">
                                        (<span itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">
                                          <meta itemprop="worstRating" content="1">
                                          <span itemprop="ratingValue"><?php echo $temp; ?></span>/
                                          <span itemprop="bestRating">5</span>
								        </span>)
                                    </div>-->
                                </div>
                                <div class="col-12">
																	<div class="verdict_wrapper">
																			<div class="row">
																					<div class="col-12 col-sm-auto mark-col">
																							<?php if($verdict['positive_or_not']) : ?>
																									<i class="fas fa-check-circle"></i>
																							<?php else : ?>
																									<i class="fas fa-exclamation-triangle"></i>
																							<?php endif; ?>
																					</div>
																					<div class="col">
																						<?php if(has_post_thumbnail()) : ?>
																							<?php the_post_thumbnail('full'); ?>
																						<?php endif; ?>
																					</div>
																					<div class="col">
																							<div class="verdict">
																									Verdict: <span
																													class="status positive"><?php echo $verdict['short']; ?></span>
																							</div>
																							<div class="text"><?php echo $verdict['text']; ?></div>
																							<div class="button_wrapper">
																									<?php if($verdict['positive_or_not']) : ?>
																											<a class="blue_btn"
																												 href="<?php echo $verdict['link_to_store']; ?>"
																												 target="_blank">
																													OPEN STORE
																											</a>
																									<?php else : ?>
																											<a class="blue_btn"
																												 href="<?php echo home_url('/category/pharmacy-reviews'); ?>">
																													FIND BEST PHARMACY
																											</a>
																									<?php endif; ?>

																							</div>
																					</div>
																					<?php if($verdict['discount_state']) : ?>
																							<div class="col-12 col-md-auto">
																									<div class="discount_coupon">
																											<div class="title">
																													DISCOUNT COUPON
																											</div>
																											<div class="discount">
																													<?php echo $verdict['discount']; ?>% OFF
																											</div>
																									</div>
																									<div class="discount_text">
																											<?php echo $verdict['discount_text']; ?>
																									</div>
																							</div>
																					<?php endif; ?>
																			</div>
																	</div>
                                </div>
                                <div class="col-12 text-col">
                                    <?php the_content(); ?>
                                </div>
																<div class="col-sm info_block-col">

																	<?php
																		$user_id = get_the_author_meta('ID');
																		$avatar_size = 40;
																		$datetime = get_the_date('Y-m-d\TH:i');;
																		$published_date = get_the_date( 'd.m.Y H:i' );
																		include get_template_directory() . '/templates/info_block.php';
																	?>

																</div>
                               <!-- <div class="col-12">
																	<div class="verdict_wrapper">
																			<div class="row">
																					<div class="col-12 col-sm-auto mark-col">
																							<?php if($verdict['positive_or_not']) : ?>
																									<i class="fas fa-check-circle"></i>
																							<?php else : ?>
																									<i class="fas fa-exclamation-triangle"></i>
																							<?php endif; ?>
																					</div>
																					<div class="col">
																							<div class="verdict">
																									Verdict: <span
																													class="status positive"><?php echo $verdict['short']; ?></span>
																							</div>
																							<div class="text"><?php echo $verdict['text']; ?></div>
																							<div class="button_wrapper">
																									<?php if($verdict['positive_or_not']) : ?>
																											<a class="blue_btn"
																												 href="<?php echo $verdict['link_to_store']; ?>"
																												 target="_blank">
																													OPEN STORE
																											</a>
																									<?php else : ?>
																											<a class="blue_btn"
																												 href="<?php echo home_url('/category/pharmacy-reviews'); ?>">
																													FIND BEST PHARMACY
																											</a>
																									<?php endif; ?>

																							</div>
																					</div>
																					<?php if($verdict['discount_state']) : ?>
																							<div class="col-12 col-md-auto">
																									<div class="discount_coupon">
																											<div class="title">
																													DISCOUNT COUPON
																											</div>
																											<div class="discount">
																													<?php echo $verdict['discount']; ?>% OFF
																											</div>
																									</div>
																									<div class="discount_text">
																											<?php echo $verdict['discount_text']; ?>
																									</div>
																							</div>
																					<?php endif; ?>
																			</div>
																	</div>
                                </div>-->
                            </div>

                            <div class="row">
                                <div class="col-12 widgets-col">
                                    <?php
                                    if(!dynamic_sidebar('pharmacy_post_content'))
                                        _e('Add widgets to sidebar', 'imedix');
                                    ?>
                                </div>
                            </div>

                            <div class="row popularity-col">
                                <div class="col item">
                                    <img src="<?php echo get_template_directory_uri(); ?>/production/images/comments_icon.png"
                                         srcset="<?php echo get_template_directory_uri(); ?>/production/images/comments_icon@2x.png 2x,
										 <?php echo get_template_directory_uri(); ?>/production/images/comments_icon@3x.png 3x"
                                         class="comments_image">
                                    <span class="text">
								<span class="description">Respond: </span><?php echo get_comments_number(); ?>
							</span>
                                </div>
                            </div>

                            <?php comments_template(); ?>
                        </article>

                    <?php endwhile;
                    wp_reset_query(); ?>
                <?php endif; ?>

            </div>
            <aside class="col-lg-3 sidebar-col d-none d-lg-block">
                <?php
                if(!dynamic_sidebar('pharmacy_post_sidebar'))
                    _e('Add widgets to sidebar', 'imedix');
                ?>
            </aside>
        </div>
    </div>
</main>

<?php get_footer(); ?>
