<?php get_header(); ?>

<?php if ( have_posts() ) : ?>

	<?php while ( have_posts() ) : the_post(); ?>

		<?php // get_template_part( 'template-parts/content', get_post_type() ); ?>

		<div class="container">
			<div class="row">
				<div class="col">

					<header class="page-header">
						<h1><?php the_title(); ?></h1>
					</header>

					<article class="page-content">
						<?php the_content(); ?>
					</article>


				</div>
			</div>
		</div>

	<?php endwhile; ?>


<?php else : ?>


<?php endif; ?>
<?php get_footer(); ?>
