<?php
/**
 * Bootstrap4Press functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Bootstrap4Press
 */

/**
 * Clean theme
 */

require get_template_directory() . '/inc/cleanup.php';

/**
 * Enqueue scripts.
 */
require get_template_directory() . '/inc/enqueue-scripts.php';

/**
 * Redirect templates.
 */
require get_template_directory() . '/inc/redirect_templates.php';

/**
 * Adding location rule to ACF.
 */
require get_template_directory() . '/inc/acf_adding_location_rule.php';

/**
 * Bootstrap 4 navwalker.
 */
// require get_template_directory() . '/inc/wp-bootstrap-navwalker.php';


/**
 * Adding theme functions.
 */
require get_stylesheet_directory() . '/inc/theme_functions.php';

/**
 * Adding common hooks.
 */
require get_stylesheet_directory() . '/inc/common_hooks.php';

/**
 * Adding widgets.
 */
require get_template_directory() . '/inc/widgets.php';

/**
 * Adding shortcodes.
 */
require get_template_directory() . '/inc/shortcodes.php';

/**
 * Adding email confirmation.
 */
require get_stylesheet_directory() . '/inc/email_confirmation.php';

/**
 * Adding restore password.
 */
require get_stylesheet_directory() . '/inc/restore_password.php';

/**
 * Adding ajax scripts.
 */
require get_template_directory() . '/inc/ajax.php';

/**
 * Adding popups.
 */
require get_template_directory() . '/templates/popups.php';

/**
 * Adding sidebars.
 */
require get_template_directory() . '/inc/sidebars.php';

/**
 * Adding pagination.
 */
require get_template_directory() . '/inc/custom_pagination.php';

/**
 * Adding comments.
 */
require get_template_directory() . '/inc/custom_comments.php';

/**
 * Custom excerpt functions.
 */
require get_stylesheet_directory() . '/inc/custom_excerpt.php';

/**
 * Custom content.
 */
require get_stylesheet_directory() . '/inc/custom_content.php';

/**
 * Count views.
 */
require get_stylesheet_directory() . '/inc/count_views.php';

/**
 * User statistic.
 */
require get_stylesheet_directory() . '/inc/user_statistic.php';

/**
 * Posts rating.
 */
require get_stylesheet_directory() . '/inc/rating.php';

/**
 * Notifications.
 */
require get_stylesheet_directory() . '/inc/notifications.php';

/**
 * Switch theme actions.
 */
require get_stylesheet_directory() . '/inc/switch_theme_actions.php';

/**
 * Custom post types.
 */
// require get_template_directory() . '/cpt/team.php';

/**
 * Add theme supports.
 */
function adding_theme_supports()
{
    add_theme_support('html5');
    add_theme_support('title-tag');
    add_theme_support('menus');
    add_theme_support('post-thumbnails');
    add_theme_support('widgets');
}

add_action('after_setup_theme', 'adding_theme_supports', 10);

/**
 * Remove top panel.
 */
function remove_admin_bar()
{
    if (!current_user_can('administrator') && !is_admin()) {
        show_admin_bar(false);
    }
}

add_action('after_setup_theme', 'remove_admin_bar');

/**
 * Register theme menu.
 */
function theme_register_menu()
{
    register_nav_menu('primary', 'Primary Menu');
    register_nav_menu('settings', 'Settings Menu');
}

add_action('after_setup_theme', 'theme_register_menu', 11);

/**
 * Change menu classes.
 */
function change_wp_nav_menu($classes, $args, $depth)
{
    if ($args->theme_location == 'primary') {
        if ($depth == 0) $classes[] = 'first_level_item';
        if ($depth == 1) $classes[] = 'second_level_item';
        if ($depth == 2) $classes[] = 'third_level_item';
        if ($depth == 3) $classes[] = 'fourth_level_item';
    }

    return $classes;
}

add_filter('nav_menu_submenu_css_class', 'change_wp_nav_menu', 10, 3);

/**
 * Adding span to menu.
 */
function span_to_nav_menu($item_output, $item, $depth, $args)
{
    if (isset($item->classes) && !empty($item->classes)) {
        if (in_array('text', (array)$item->classes)) {
            $item_output = '<span class="title">' . $item->title . '</span>';
        } elseif (in_array('disable', (array)$item->classes)) {
            $item_output = '';
        }
    }
    return $item_output;
}

add_filter('walker_nav_menu_start_el', 'span_to_nav_menu', 10, 4);

/**
 * Allow shortcodes in menu in title.
 */
add_filter('wp_nav_menu_items', 'do_shortcode');

/**
 * Allows shortcodes in Custom HTML widget.
 */
add_filter('widget_text', 'do_shortcode');

/**
 * Allow HTML and shortcodes in menu in description.
 */
remove_filter('nav_menu_description', 'strip_tags');
function my_plugin_wp_setup_nav_menu_item($menu_item)
{
    if (!is_admin() && isset($menu_item->post_type)) {
        if ('nav_menu_item' == $menu_item->post_type) {
            $menu_item->description = do_shortcode($menu_item->post_content);
        }
    }

    return $menu_item;
}

add_filter('wp_setup_nav_menu_item', 'my_plugin_wp_setup_nav_menu_item');

/**
 * Extending menu walker.
 */
class Custom_Sublevel_Walker extends Walker_Nav_Menu
{
    function start_lvl(&$output, $depth = 0, $args = array())
    {
        $indent = str_repeat("\t", $depth);
        if ($depth == 0) {
            $output .= "\n$indent<div class='sub-menu-wrap first_level_menu'><ul class='sub-menu row'>\n";
        } elseif ($depth == 1) {
            $output .= "\n$indent<div class='sub-menu-wrap second_level_menu'><ul class='sub-menu row'>\n";
        } elseif ($depth == 2) {
            $output .= "\n$indent<div class='sub-menu-wrap third_level_menu'><ul class='sub-menu'>\n";
        } else {
            $output .= "\n$indent<div class='sub-menu-wrap fourth_level_menu'><ul class='sub-menu'>\n";
        }
    }

    function end_lvl(&$output, $depth = 0, $args = array())
    {
        $indent = str_repeat("\t", $depth);
        $output .= "$indent</ul></div>\n";
    }
}

/**
 * Changing default user avatar.
 */
function add_default_avatar_option($avatars)
{
    $url = get_template_directory_uri() . '/production/images/user_thumbnail-40x40.png';
    $avatars[$url] = __('Website avatar', 'imedix');
    return $avatars;
}

add_filter('avatar_defaults', 'add_default_avatar_option');

function get_avatar_url_func($url, $id_or_email, $args)
{
    $user = false;

    // Process the user identifier.
    if (is_numeric($id_or_email)) {
        $user = get_user_by('id', absint($id_or_email));
    } elseif (is_string($id_or_email)) {
        $user = get_user_by('email', $id_or_email);
    } elseif ($id_or_email instanceof WP_User) {
        // User Object
        $user = $id_or_email;
    } elseif ($id_or_email instanceof WP_Post) {
        // Post Object
        $user = get_user_by('id', (int)$id_or_email->post_author);
    } elseif ($id_or_email instanceof WP_Comment) {
        $allowed_comment_types = apply_filters('get_avatar_comment_types', array('comment'));
        if (!empty($id_or_email->comment_type) && !in_array($id_or_email->comment_type, (array)$allowed_comment_types)) {
            $args['url'] = false;
            return apply_filters('get_avatar_data', $args, $id_or_email);
        }

        if (!empty($id_or_email->user_id)) {
            $user = get_user_by('id', (int)$id_or_email->user_id);
        }
    }

    if (!$user) return $url;
    global $wpdb;
    $attachment_id = get_user_meta($user->ID, $wpdb->get_blog_prefix() . 'user_avatar', true);
    if (empty($attachment_id)) return $url;
    $image_attributes = wp_get_attachment_image_src($attachment_id, array($args['width'], $args['height']));
    if (!$image_attributes) return $url;

    return $image_attributes[0];

}

add_filter('get_avatar_url', 'get_avatar_url_func', 10, 3);

/**
 * Not to replace html in widget titles..
 */
remove_filter('widget_title', 'esc_html');

/**
 * ACF option pages.
 */
// if( function_exists('acf_add_options_page') ) {

//  	// add parent
// 	$parent = acf_add_options_page(array(
// 		'page_title' 	=> 'Theme options'
// 	));


// 	// add sub page
// 	acf_add_options_sub_page(array(
// 		'page_title' 	=> 'Theme options',
// 		'menu_title' 	=> 'Options',
// 		'parent_slug' 	=> $parent['menu_slug'],
// 	));

// }

/**
 * Display Contact Form 7 messages in popup.
 */
function ContactForm7_popup()
{

    $return = <<<EOT
	<script>
		jQuery(document).ready(function($) {
			$(".wpcf7-form input[type='submit'], .wpcf7-form button").click(function(event) {
				$( document ).one( "ajaxComplete", function(event, xhr, settings) {
					var data = xhr.responseText;
					var jsonResponse = JSON.parse(data);
					// console.log(jsonResponse);
					if(! jsonResponse.hasOwnProperty('into') || $('.wpcf7' + jsonResponse.into).length === 0) return;
					// if(jsonResponse["status"] === 'mail_sent') {}
					// alert(jsonResponse.message);
					$.fancybox.open(
						'<div class="message">' + jsonResponse.message + '</div>',
						{
							smallBtn : true,
							toolbar : false
						}
					);
				});
			});
		});
	</script>
	<style>
		div.wpcf7-response-output, div.wpcf7-validation-errors { display: none !important; }
		span.wpcf7-not-valid-tip { display: none; }
		input[aria-invalid="true"], select[aria-invalid="true"] { border-color: #ff2c00; // background-color: rgba(153,0,0,0.3); }
	</style>

EOT;
    echo $return;
}

add_action('wp_footer', 'ContactForm7_popup', 20);

/**
 * Notifications.
 */
function adding_image_sizes() {
    add_image_size( 'pharmacy_reviews', 312, 180, true );
//    add_image_size( '1200', 1200, '', false );
}
add_action( 'after_setup_theme', 'adding_image_sizes' );

/**
 * Notifications.
 */
function thumbnail_upscale($default, $orig_w, $orig_h, $new_w, $new_h, $crop)
{
    if (!$crop) return null; // let the wordpress default function handle this

    $aspect_ratio = $orig_w / $orig_h;
    $size_ratio = max($new_w / $orig_w, $new_h / $orig_h);

    $crop_w = round($new_w / $size_ratio);
    $crop_h = round($new_h / $size_ratio);

    $s_x = floor(($orig_w - $crop_w) / 2);
    $s_y = floor(($orig_h - $crop_h) / 2);

    return array(0, 0, (int)$s_x, (int)$s_y, (int)$new_w, (int)$new_h, (int)$crop_w, (int)$crop_h);
}

add_filter('image_resize_dimensions', 'thumbnail_upscale', 10, 6);

add_filter('wpseo_schema_article', 'change_article_meta' );
function change_article_meta($data) { 
	$verdict = get_field('verdict');
	$link = get_the_permalink();
	
	$drugs_page   = 'https://www.imedix.com/drugs';
	$pos = strpos($link, $drugs_page);
	
	if ($pos !== false) {
		$data['author']['name'] = get_the_author();
		
		unset($data['publisher']);
		
		$thumbnail_url = get_the_post_thumbnail_url(get_the_ID()); 
		if($thumbnail_url) {
			$data['image'] = $thumbnail_url;
		}
		
		$data['publisher']['@type'] = 'Organization';
		$data['publisher']['name'] = 'Imedix';
		$data['publisher']['logo']['@type'] = 'ImageObject';
		$data['publisher']['logo']['url'] = 'https://www.imedix.com/wp-content/themes/imedix/production/images/logo.png';		
	}
/* 	if ($pos !== false) {
		$data['@type'] = 'Product'; 
		$data['name'] = $data['headline']; 
		$data['description'] = $verdict['text']; 
		$data['brand'] = !empty($verdict['brand']) ? $verdict['brand'] : $verdict['title'];
		$data['sku'] = $data['mpn'] = !empty($verdict['sku']) ? $verdict['sku'] : get_the_ID();
		
		$post_categories = wp_get_post_categories(get_the_ID(), array('fields' => 'all'));
		
		if ($post_categories) {
			$data['category'] = $post_categories[0]->name;
		}
		
		$thumbnail_url = get_the_post_thumbnail_url(get_the_ID()); 
		if($thumbnail_url) {
			$data['image'] = $thumbnail_url;
		}
		
    $data['offers'] = array(
			"@type" => "AggregateOffer",
			"availability" => "http://schema.org/InStock",
			"itemCondition" => "http://schema.org/NewCondition",
			"lowPrice" => $verdict['price'],
			"highPrice" => isset($verdict['highPrice']) && $verdict['highPrice']!='' ? $verdict['highPrice'] : $verdict['price'],
			"offerCount" => isset($verdict['offerCount']) ? $verdict['offerCount'] : 100,
			"priceCurrency" => "USD",	
    );
		
		$comments = get_comments('post_id='.get_the_ID().'&status=approve');
		$rating_default = !empty($verdict['rating']) ? $verdict['rating'] : 4;
		$rating_count = 0;
		$rating_sum = 0;
		
		if ($comments) {
			$data['review'] = array();
			$i = 0;
			foreach($comments as $key=>$comment){
				if ($comment->comment_approved == 1) {
					$rating = get_comment_meta($comment->comment_ID, 'rating-' . get_the_ID(), true);
					
					if (!$rating && $comment->comment_parent==0) $rating = $rating_default;
					
					$data['review'][$i] = array(
						"@type" => "Review",
						"author" => $comment->comment_author,
						"datePublished" => $comment->comment_date,
						"reviewBody" => $comment->comment_content
					);
					if ($rating && $comment->comment_parent==0) {
						$rating_sum += $rating;
						$rating_count++;
						
						$data['review'][$i]['reviewRating'] = array(
							"@type" => "Rating",
							"bestRating" => "5",
							"ratingValue" => $rating,
							"worstRating" => "1"
						);
					}
					$i++;
				}
			}
		}		
		
		if ($rating_sum==0) $rating_sum = $rating_default;
		if ($rating_count==0) $rating_count = 1;
		
		$rating_value = !empty($rating_count) && !empty($rating_sum) ? round($rating_sum / $rating_count, 2) : $rating_default;
		
		$data['aggregateRating'] = array(
			"@type" => "AggregateRating",
			"bestRating" => "5",
			"ratingValue" => $rating_value,
			"reviewCount" => $rating_count
		);
		
		global $post;

		// Код виджета
		$options = array(
			'orderby' => array('date' => 'DESC'),
			'posts_per_page' => 4,
		);

		$options['post_type'] = 'post';
		$options['cat']       = get_the_category()[0]->term_id;
		$options['post__not_in'] = array($post->ID);

		// d( get_the_category() );
		$loop = new WP_Query($options);		
		
		if ($loop->have_posts()) {
			
			$data['isRelatedTo'] = array();
			while ($loop->have_posts()) : $loop->the_post();
				$product = array(
					'@type' => 'Product',
					'name' => get_the_title(),
					'url' => get_permalink()
				);
				$data['isRelatedTo'][] = $product;
			endwhile;
			wp_reset_query();
		}
		
		unset($data['author']);
		unset($data['publisher']);
		unset($data['commentCount']);
		unset($data['isPartOf']);
		unset($data['headline']);
		unset($data['datePublished']);
		unset($data['dateModified']);
		unset($data['articleSection']);
		unset($data['mainEntityOfPage']);
	} */
	
	$qa_page   = 'https://www.imedix.com/qa';
	$pharmacy_reviews_page   = 'https://www.imedix.com/pharmacy-reviews';
	$pos_qa = strpos($link, $qa_page);
	$pos_pharmacy_reviews = strpos($link, $pharmacy_reviews_page);
	
	if ($pos_qa !== false) {
		$data = array();
		return $data;
	}
		
	if ($pos_pharmacy_reviews !== false) {		
		$data = array();
		return $data;
		
		$thumbnail_url = get_the_post_thumbnail_url(get_the_ID()); 
		if($thumbnail_url) {
			$data['image'] = $thumbnail_url;
		} else {
			$data['image'] = 'https://www.imedix.com/wp-content/themes/imedix/production/images/logo.png';
		}
		
		$data['author']['name'] = get_the_author();
		
		unset($data['publisher']);
		
		$data['publisher']['@type'] = 'Organization';
		$data['publisher']['name'] = 'Imedix';
		$data['publisher']['logo']['@type'] = 'ImageObject';
		$data['publisher']['logo']['url'] = 'https://www.imedix.com/wp-content/themes/imedix/production/images/logo.png';
		
		unset($data['commentCount']);
		unset($data['isPartOf']);
		unset($data['articleSection']);
		unset($data['article']);
	}
	
  return $data; 
}
add_filter('wpseo_schema_webpage', 'remove_webpage_meta' );
function remove_webpage_meta($data) { 
	$data = array();
	return $data;
}
add_filter('wpseo_schema_website', 'remove_website_meta' );
function remove_website_meta($data) { 
	$data = array();
	return $data;
}
add_filter('wpseo_schema_author', 'remove_author_meta' );
function remove_author_meta($data) { 
	$data = array();
	return $data;
}
add_filter('wpseo_schema_organization', 'remove_organization_meta' );
function remove_organization_meta($data) { 
	$data = array();
	return $data;
}
add_filter('wpseo_schema_mainimage', 'remove_mainimage_meta' );
function remove_mainimage_meta($data) { 
	$data = array();
	return $data;
}

$doctor = get_role( 'doctor' );
if (!$doctor) {
	$author = get_role( 'author' );
	add_role( 'doctor', 'Doctor', $author->capabilities );
}

$doctor_args = array(
	'labels' => false,
	'public' => true,
	'show_in_menu' => false,
	'menu_icon' => 'dashicons-feedback',
	'menu_position' => 5,
	'has_archive' => false,
	'supports' => array('title','editor','thumbnail','comments ')
);
register_post_type('doctor_post', $doctor_args);

add_action('template_redirect', 'doctor_redirect');
function doctor_redirect() {
/*   if ( is_main_query() && is_single() && !empty(get_post_type()) && get_post_type() === 'doctor_post') {
		global $post;
		$doctor_id = get_post_meta($post->ID, 'doctor_id', true);
		wp_safe_redirect( '/users/user/'.$doctor_id, 301 );
		exit(); 
  } */
	if (is_author()) {
		global $wp_query;
		$user = get_user_by( 'slug', $wp_query->query['author_name'] );
		//wp_safe_redirect( '/users/user/'.$user->ID, 301 );
		//exit(); 
	} 
}

function add_meta_for_search_excluded()
{
	if (is_author()) {
		global $wp_query;
		$user = get_user_by( 'slug', $wp_query->query['author_name'] );
		if (in_array( 'doctor', $user->roles, true) || in_array( 'author', $user->roles, true)) {
			
		} else {
			echo '<meta name="robots" content="noindex,nofollow" />', "\n";
		} 
	} 	
}
add_action('wp_head', 'add_meta_for_search_excluded');

function return_canon () {
	$canon_page = get_pagenum_link(1);
	return $canon_page;
}

function canon_paged() {
	if (is_paged()) {
		add_filter( 'wpseo_canonical', 'return_canon' );
	}
}
add_filter('wpseo_head','canon_paged');