	</div><!-- #site-content -->

		<footer id="site-footer">
			<div class="first_footer">
				<div class="container">
					<div class="row">
						<div class="col-md-4 col-lg-3 first-col d-none d-md-block">
							<?php
								if ( ! dynamic_sidebar('footer_1_sidebar') ) _e('Add widgets to sidebar', 'imedix');
							?>
						</div>
						<div class="col-md-4 col-lg-3 second-col d-none d-md-block">
							<?php
								if ( ! dynamic_sidebar('footer_2_sidebar') ) _e('Add widgets to sidebar', 'imedix');
							?>
						</div>
						<div class="col-md-4 col-lg-6 third-col">
							<?php
								if ( ! dynamic_sidebar('footer_3_sidebar') ) _e('Add widgets to sidebar', 'imedix');
							?>
						</div>
					</div>
				</div>
			</div>
			<div class="second_footer">
				<div class="container">
					<div class="row">
						<div class="col content">
							<?php
								if ( ! dynamic_sidebar('footer_4_sidebar') ) _e('Add widgets to sidebar', 'imedix');
							?>
						</div>
					</div>
				</div>
			</div>

	</footer><!-- #site-footer -->

    <?php
     if(is_category_post('drugs')) {
        get_template_part('templates/drugs_sticky_block');
     } else if(is_category_post('pharmacy-reviews')) {
         get_template_part('templates/pharmacies_sticky_block');
     }
     ?>

</div><!-- #site-wrapper -->

<?php wp_footer(); ?>

<script>
	jQuery(document).ready(function($){
		jQuery('body').delegate('.tabs_btns_js .item_link.item_link__js' , 'click', function(){
			$('.tabs_btns_js .item_link').removeClass('active');
			$('.tabs_content .tabs-block').removeClass('active');
			$(this).addClass('active');
			$('.tabs_content .tabs-block'+$(this).attr('href')).addClass('active');
			return false;
		});
	});
</script>

<script type="application/ld+json">
{  
  "@context": "http://schema.org",
  "@type": "Person",
  "name": "iMedix ", 
  "jobTitle": "Medical Director",
  "image": "https://www.imedix.com/wp-content/themes/imedix/production/images/logo.png",
  "knowsAbout": [  
            {
          "@type":"thing",
          "name":"Digital Health"
        },
        {  
      "@type": "thing",
      "name": "Medical"
    },
    {  
      "@type": "thing",
      "name": "healthcare"
    },
    {  
      "@type": "thing",
      "name": "consulting"
    }
  ],
  "sameAs": [
      ]
}
</script>

</body>
</html>
