<?php get_header(); ?>

<main>
	<div class="container">
		<div class="row">
			<aside class="col-lg-3 sidebar-settings d-none d-lg-block">

				<?php include get_template_directory() . '/templates/settings_sidebar.php'; ?>

			</aside>
			<div class="col-lg-9">
				<div class="content-settings notifications">
					<div class="events">
						<?php
							$notifications = get_notifications( 10 );
							$total = $notifications->max_num_pages;
							$found_notifications = $notifications->found_notifications;
							$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
							$count = count( $notifications->notifications );
						?>
						<div class="title-wr">
							<h1 class="page_title">
								<?php echo $found_notifications; ?> Notifications
							</h1>
							<a href="#" class="marked_all_link ajax_read_notifications mark_as_read" data-nonce="<?php echo wp_create_nonce('ajax_read_notifications'); ?>">Mark All As Read</a>
						</div>
						<div class="content">
							<?php foreach ( $notifications->notifications as $notification ) : ?>
								<?php
									$action = $notification->action;
									$user_id = $notification->user_action_id;
									$user = get_user_by( 'ID', $user_id );
									$date = $notification->date;
									$date = time_elapsed_string( $date );
									switch ( $action ) {
										case 'answer':
											$post_id = $notification->post_action_id;
											$post_title = get_post_field( 'post_title', $post_id );
											$comment_id = $notification->comment_action_id;
											$href = get_post_field( 'guid', $post_id ) . '#comment-' . $comment_id;
											$text = '<span class="q_type">answered in your topic</span>
														<a href="" class="q_title">“' . $post_title . '”</a>';
											break;

										case 'comment':
											$post_id = $notification->post_action_id;
											$post_title = get_post_field( 'post_title', $post_id );
											$comment_id = $notification->comment_action_id;
											$href = get_post_field( 'guid', $post_id ) . '#comment-' . $comment_id;
											$text = '<span class="q_type">commented in your topic</span>
														<a href="" class="q_title">“' . $post_title . '”</a>';
											break;

										case 'upvote':
											$post_id = $notification->post_action_id;
											$post_title = get_post_field( 'post_title', $post_id );
											$comment_id = $notification->comment_action_id;
											$comment = get_comment( $comment_id );
											$what = $comment->comment_parent == 0 ? 'answer' : 'comment';
											$href = get_post_field( 'guid', $post_id ) . '#comment-' . $comment_id;
											$text = 'upvoted your ' . $what . ' in topic <span>“' . $post_title . '”</span>';
											$text = 'upvoted your ' . $what . ' in topic <span>“' . $post_title . '”</span>';
											$text = '<span class="q_type">upvoted your ' . $what . ' in topic </span>
														<a href="" class="q_title">“' . $post_title . '”</a>';
											break;

										case 'follower':
											$text = 'follow you';
											$href = get_author_posts_url($user_id);
											break;

										default:
											continue;
											break;
									}
								?>
								<div class="row">
									<div class="col item-col">

										<div class="notification-item <?php echo $notification->readed == 1 ? 'readed' : 'unreaded'; ?>">
											<div class="item_wr">

												<div class="user_photo">
													<a href="" class="photo_link">
														<div class="user_thumbnail">
															<?php echo get_avatar( $user->user_email, '24' ); ?>
														</div>
													</a>
												</div>

												<div class="user_message">
													<a href="" class="user_name_link">
														<?php echo $user->display_name; ?>
													</a>
													<?php echo $text; ?>
													<span class="q_date"><?php echo $date; ?></span>
												</div>

											</div>

										</div>

									</div>
								</div>
							<?php endforeach; ?>
						</div>

						<?php if ( $count == 0 ) : ?>
							<div class="row">
								<div class="col item-col">
									You haven't got notifications
								</div>
							</div>
						<?php endif; ?>
					</div>
				</div>
				<div class="row">
					<div class="col">
						<?php
						 $paginate = paginate_links(array(
								// 'total' => 8,
								// 'current' => 1,
						     'total' => $total,
						     'current' => $paged,
						     // 'base' => URI . '/my-account/manage-tours/' . '%_%',
						     // 'format' => '%#%',
						     'type' => 'array',
						     'end_size' => 1,
						     'mid_size' => 1,
						     'prev_text' => '<i class="fas fa-caret-left"></i>',
						     'next_text' => '<i class="fas fa-caret-right"></i>',
						 ));
						 // die(var_dump($paginate));
						display_pagination( $paginate );
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>

<?php get_footer(); ?>
