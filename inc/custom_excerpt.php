<?php
// Function to cut the string
function cut_string($string, $count, $addition=''){
  if(mb_strlen($string, 'utf-8')<=$count){
	  return $string;
  }else{
	  $string=mb_substr($string, 0, $count,'utf-8');

	  if (preg_match("/^[^0-9a-zа-я]$/iu", substr($string, -1))) {
		  $string=preg_replace("/[^0-9a-zа-я]+$/iu", "", $string);
	  } else {
		  $string=preg_replace("/[0-9a-zа-я]+$/iu", "", $string);
	  }

	  return $string . $addition;
  }
}

// Function returns cut string
function return_excerpt($count, $addition){
	global $post;
	$excerpt = get_the_content();
	$excerpt = preg_replace('#<style[^>]*?>.*?</style>#si', '', $excerpt);
	$excerpt = preg_replace('#<script[^>]*?>.*?</script>#si', '', $excerpt);
	$excerpt = strip_shortcodes($excerpt);
	$excerpt = strip_tags($excerpt);
	$excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));
	if($addition != '') $addition = '... <a href="' . get_permalink() . '">' . __('more', 'imedix') . '</a>';
	$excerpt = cut_string($excerpt, $count, $addition);

	return $excerpt;
}

// Function to echo cut string
function echo_excerpt($count, $addition){
	echo return_excerpt($count, $addition);
}
