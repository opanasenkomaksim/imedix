<?php

/**
 * Adding ajax scripts.
 */

require get_template_directory() . '/inc/ajax/login.php';

if( !is_user_logged_in() ) {
	require get_template_directory() . '/inc/ajax/registration.php';
} else {
	require get_template_directory() . '/inc/ajax/comment_votes.php';
	require get_template_directory() . '/inc/ajax/best_comment.php';
	require get_template_directory() . '/inc/ajax/question_editor.php';
	require get_template_directory() . '/inc/ajax/get_question.php';
	require get_template_directory() . '/inc/ajax/remove_question.php';
	require get_template_directory() . '/inc/ajax/edit_comment.php';
	require get_template_directory() . '/inc/ajax/add_question_to_favorites.php';
	require get_template_directory() . '/inc/ajax/remove_question_from_favorites.php';
	require get_template_directory() . '/inc/ajax/add_user_to_favorites.php';
	require get_template_directory() . '/inc/ajax/remove_user_from_favorites.php';
	require get_template_directory() . '/inc/ajax/account_settings.php';
	require get_template_directory() . '/inc/ajax/email_notification_settings.php';
	require get_template_directory() . '/inc/ajax/read_notifications.php';
}
