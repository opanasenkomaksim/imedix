<?php
function setPostViews() {
	global $post;
	if(!$post) return;
	$post_id = $post->ID;
  $count_key = 'post_views_count';
  if(!isset($_COOKIE['post_views_count_' . $post_id ]) || $_COOKIE['post_views_count_' . $post_id ] != true) {
  	$count = display_views(false);

    $count++;
    update_post_meta($post_id, $count_key, $count);
	  // 20 minutes
	  setcookie('post_views_count_' . $post_id, true, time() + (60 * 20), '/', get_domain());

	  do_action('after_post_views_set');
  }
}

function count_post_veiws() {
	setPostViews();
}
add_action('wp', 'count_post_veiws');

function display_views($echo = true) {
		global $post;
		$post_id = $post->ID;
		$count_key = 'post_views_count';
  	$count = get_post_meta($post_id, $count_key, true);
	  if(empty($count)) $count = 0;

	  if(!$echo) {
	  	return $count;
	  } else {
	  	echo $count;
	  }
}
