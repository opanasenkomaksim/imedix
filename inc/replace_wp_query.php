<?php

	function theme_categories_filter($query) {

		if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) return;
		if ( is_admin() ) return;
		if ( !$query->is_main_query( ) ) return;
		if ( !$query->get( 'cat' ) && $query->get('page_id') != get_option('page_on_front') ) return;


		// Get original meta query
		// $meta_query = $query->get('meta_query');
		// $tax_query = $query->get('tax_query');

		$meta_query = array();
		$tax_query = array();

		$filters = array( 'Interesting', 'Top Week', 'Top Month', 'All' );
		$filters_slugs = array_map( 'strtolower', $filters );
		$filters_slugs = str_replace( ' ', '-', $filters_slugs );
		$filter = ! empty( $_GET[ 'filter' ] ) && in_array( $_GET[ 'filter' ], $filters_slugs ) ? $_GET[ 'filter' ] : 'all';


		if ( $filter == 'top-week' ) {
			function filter_where_date($where = '') {
			    //posts in the last 7 days
			    $where .= " AND post_date > '" . date('Y-m-d', strtotime('-7 days')) . "'";
			    return $where;
			}
			add_filter('posts_where', 'filter_where_date');
		}

		if ( $filter == 'top-month' ) {
			function filter_where_date($where = '') {
			    //posts in the last 30 days
			    $where .= " AND post_date > '" . date('Y-m-d', strtotime('-30 days')) . "'";
			    return $where;
			}
			add_filter('posts_where', 'filter_where_date');
		}

		if( is_user_logged_in() ) {
			function filter_where_status( $where ) {
			    $where .= " OR (post_author = " . get_current_user_id() . " AND
			    	post_status = 'pending'
			    ) ";
			    return $where;
			}
			add_filter('posts_where', 'filter_where_status');
		}

		$options = array();

		if( $query->get('page_id') == get_option('page_on_front') ) {
			$options['category_name'] = 'qa';
			// $query->set( 'category_name', 'qa' );
			// $term = get_term_by('slug', 'qa', 'category');
			// $query->set( 'cat', $term->term_id );
		}

		if ( $filter == 'interesting' || $filter == 'top-week' || $filter == 'top-month' ) {
			$options['orderby'] = array ( 'meta_value_num' => 'DESC', 'date' => 'DESC' );
			$options['meta_key'] = 'post_rating';
		}
		$default = array(
			'post_type' => 'post',
			// 'paged' => $paged,
			'orderby' => array ( 'date' => 'DESC' ),
			// 'posts_per_page' => 2,
		);

		$meta_query = array_merge( $default, $options);

		// die(var_dump($meta_query));






		if(count($meta_query)>1 && !isset($meta_query['relation'])) {
			$meta_query['relation'] = 'AND';
		}
		if(count($tax_query)>1 && !isset($tax_query['relation'])) {
			$tax_query['relation'] = 'AND';
		}

    $query->set( 'meta_query', $meta_query );
    $query->set( 'tax_query', $tax_query );
    // $query->set( 'order', 'DESC' );

		// print_r($GLOBALS['wp_query']);
		// die();
  }
  add_action('pre_get_posts', 'theme_categories_filter', 20);

