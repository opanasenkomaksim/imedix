<?php
function set_rating() {
	global $post;
	if(!$post) return;
	$post_id = $post->ID;

	$views = display_views( false );
	$comments = get_comments_number( $post_id );

	$rating = $views + 5 * $comments;

	$rating_key = 'post_rating';
	update_post_meta( $post_id, $rating_key, $rating );
}
add_action( 'after_post_views_set', 'set_rating' );
add_action( 'wp_insert_comment', 'set_rating' );
