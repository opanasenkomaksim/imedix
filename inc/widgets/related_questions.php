<?php
/**
 * Добавление нового виджета Foo_Widget.
 */
class Related_Questions_Widget extends WP_Widget {

	// Регистрация виджета используя основной класс
	function __construct() {
		// вызов конструктора выглядит так:
		// __construct( $id_base, $name, $widget_options = array(), $control_options = array() )
		parent::__construct(
			'related_questions', // ID виджета, если не указать (оставить ''), то ID будет равен названию класса в нижнем регистре
			'/*** STL - related questions ***/',
			array( 'description' => 'Related questions', /*'classname' => 'my_widget',*/ )
		);
	}

	/**
	 * Вывод виджета во Фронт-энде
	 *
	 * @param array $args     аргументы виджета.
	 * @param array $instance сохраненные данные из настроек
	 */
	function widget( $args, $instance ) {
		$widget_id = $args['widget_id'];

		$title = apply_filters( 'widget_title', get_field( 'title', 'widget_' . $widget_id ) );

		echo $args['before_widget'];
		if ( ! empty( $title ) ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}

		// Код виджета
		$count = get_field( 'count', 'widget_' . $widget_id );
		if ( ! is_single() ) return;
		if ( ! is_category_post( 'qa' ) && ! is_category_post( 'drugs' ) ) return;
		?>

		<?php
			if ( is_category_post( 'qa' ) ) {
				$terms = wp_get_post_categories( get_the_ID() );
			} elseif ( is_category_post( 'drugs' ) ) {
				$terms = get_field( 'related_to_question_categories', get_the_ID() );
			}
			if ( empty( $terms ) ) {
				$term = get_term_by( 'slug', 'qa', 'category' );
				if ( $term ) $terms = $term->term_id;
			}
			$options = array(
				'post_type' => 'post',
				'cat' => $terms,
				'orderby' => array ( 'meta_value_num' => 'DESC', 'date' => 'DESC' ),
				'meta_key' => 'post_rating',
				'posts_per_page' => $count
			);
			$loop = new WP_Query( $options );
		?>

		<?php if ( $loop->have_posts() ) : ?>
		<ul class="list-unstyled" id="related_questions">
			<?php while ($loop->have_posts()) : $loop->the_post(); ?>
			<li class="item">
				<a href="<?php the_permalink(); ?>" class="item_link">
					<?php the_title(); ?>
				</a>
			</li>
			<?php endwhile; wp_reset_query(); ?>
		</ul>
		<?php endif; ?>

		<?php

		echo $args['after_widget'];
	}

	/**
	 * Админ-часть виджета
	 *
	 * @param array $instance сохраненные данные из настроек
	 */
	function form( $instance ) {

		?>

		<?php
	}

	/**
	 * Сохранение настроек виджета. Здесь данные должны быть очищены и возвращены для сохранения их в базу данных.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance новые настройки
	 * @param array $old_instance предыдущие настройки
	 *
	 * @return array данные которые будут сохранены
	 */
	function update( $new_instance, $old_instance ) {
		// $instance = array();
		// $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

		return $new_instance;
	}

}
// конец класса Foo_Widget

// регистрация Foo_Widget в WordPress
function register_related_questions_widget() {
	register_widget( 'Related_Questions_Widget' );
}
add_action( 'widgets_init', 'register_related_questions_widget' );
