<?php
/**
 * Добавление нового виджета Foo_Widget.
 */
class Top_Categories_Widget extends WP_Widget {

	// Регистрация виджета используя основной класс
	function __construct() {
		// вызов конструктора выглядит так:
		// __construct( $id_base, $name, $widget_options = array(), $control_options = array() )
		parent::__construct(
			'top_categories', // ID виджета, если не указать (оставить ''), то ID будет равен названию класса в нижнем регистре
			'/*** STL - top categories ***/',
			array( 'description' => 'Top categories', /*'classname' => 'my_widget',*/ )
		);
	}

	/**
	 * Вывод виджета во Фронт-энде
	 *
	 * @param array $args     аргументы виджета.
	 * @param array $instance сохраненные данные из настроек
	 */
	function widget( $args, $instance ) {
		$widget_id = $args['widget_id'];

		$title = apply_filters( 'widget_title', get_field( 'title', 'widget_' . $widget_id ) );

		echo $args['before_widget'];
		if ( ! empty( $title ) ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}

		// Код виджета
		$count = get_field( 'count', 'widget_' . $widget_id );
		$category = get_category_by_slug( 'qa' );
		$categories = get_categories( array(
			'child_of'       => $category->term_id,
			'orderby'      => 'count',
			'order'        => 'DESC',
			'number'       => $count,
			'taxonomy'     => 'category',
			'pad_counts'   => true,
			// полный список параметров смотрите в описании функции http://wp-kama.ru/function/get_terms
		) );
		?>

		<?php if( count( $categories ) > 0 ) : ?>
			<ul class="list-unstyled" id="top_categories">
				<?php foreach ($categories as $category) : ?>
					<li class="item">

						<div class="open_favorites_menu right">
							<a href="<?php echo get_category_link( $category->term_id ); ?>" class="item_link">
								<?php echo $category->name; ?>
							</a>
							<?php display_favorites_menu( 'category', $category->term_id ); ?>
						</div>

					</li>
				<?php endforeach; ?>
			</ul>

			<a href="<?php echo home_url( '/category/qa/' ); ?>" class="more_link">
				All  Category <i class="fas fa-arrow-right"></i>
			</a>
		<?php endif; ?>

		<?php

		echo $args['after_widget'];
	}

	/**
	 * Админ-часть виджета
	 *
	 * @param array $instance сохраненные данные из настроек
	 */
	function form( $instance ) {

		?>

		<?php
	}

	/**
	 * Сохранение настроек виджета. Здесь данные должны быть очищены и возвращены для сохранения их в базу данных.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance новые настройки
	 * @param array $old_instance предыдущие настройки
	 *
	 * @return array данные которые будут сохранены
	 */
	function update( $new_instance, $old_instance ) {
		// $instance = array();
		// $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

		return $new_instance;
	}

}
// конец класса Foo_Widget

// регистрация Foo_Widget в WordPress
function register_top_categories_widget() {
	register_widget( 'Top_Categories_Widget' );
}
add_action( 'widgets_init', 'register_top_categories_widget' );
