<?php
/**
 * Добавление нового виджета Foo_Widget.
 */
class Top_Pharmacies_Widget extends WP_Widget {

	// Регистрация виджета используя основной класс
	function __construct() {
		// вызов конструктора выглядит так:
		// __construct( $id_base, $name, $widget_options = array(), $control_options = array() )
		parent::__construct(
			'top_pharmacies', // ID виджета, если не указать (оставить ''), то ID будет равен названию класса в нижнем регистре
			'/*** STL - top pharmacies ***/',
			array( 'description' => 'Top Pharmacies', /*'classname' => 'my_widget',*/ )
		);
	}

	/**
	 * Вывод виджета во Фронт-энде
	 *
	 * @param array $args     аргументы виджета.
	 * @param array $instance сохраненные данные из настроек
	 */
	function widget( $args, $instance ) {
		$widget_id = $args['widget_id'];

		$title = apply_filters( 'widget_title', get_field( 'title', 'widget_' . $widget_id ) );

		echo $args['before_widget'];
		if ( ! empty( $title ) ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}

		$now = new DateTime( 'now' );
		$now = $now->format( 'j-F-Y' );
		?>

		<div class="date">UPDATED <?php echo $now; ?></div>

		<?php
		$pharmacies = get_field( 'pharmacies', 'widget_' . $widget_id );
		?>

		<?php if ( is_array( $pharmacies ) && count( $pharmacies) > 0 ) : ?>

		<ul class="list-unstyled" id="top_pharmacies">

	  	<?php
	  		$i=1;
	  		global $post;
	  	?>
	  	<?php foreach ($pharmacies as $pharmacy) : ?>
	  	<?php
	  		$post = $pharmacy['pharmacy'];
	  		setup_postdata( $post );
	  	?>

			<li class="item">
				<?php $thumbnail = get_the_post_thumbnail( get_the_ID(), 'medium', array( 'class' => 'article_image' ) ); ?>
				<?php if ( $thumbnail ) : ?>
				<div class="thumbnail">
					<?php echo $thumbnail; ?>
				</div>
				<?php endif; ?>
				<div class="body">
					<?php
						$verdict = get_field( 'verdict' );
					?>
					<div class="title">
						<?php echo $i . '. ' . get_the_title(); ?>
					</div>
					<div class="text-center">
						<a href="<?php the_permalink(); ?>" class="gray_btn">
							Review
						</a>
						<a href="<?php echo $verdict[ 'link_to_store' ]; ?>" class="blue_btn">
							OPEN STORE
						</a>
					</div>
					<?php if( $verdict[ 'discount_state' ] ) : ?>
					<div class="discount_coupon">
						<div class="title">
							DISCOUNT COUPON
						</div>
						<div class="discount">
							<?php echo $verdict[ 'discount' ]; ?>% OFF
						</div>
					</div>
					<?php endif; ?>
					<div class="text">
						<?php echo $verdict[ 'text' ]; ?>
					</div>
				</div>
			</li>

			<?php $i++; endforeach; wp_reset_postdata(); ?>

		</ul>
		<?php endif; ?>

		<?php

		echo $args['after_widget'];
	}

	/**
	 * Админ-часть виджета
	 *
	 * @param array $instance сохраненные данные из настроек
	 */
	function form( $instance ) {

		?>

		<?php
	}

	/**
	 * Сохранение настроек виджета. Здесь данные должны быть очищены и возвращены для сохранения их в базу данных.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance новые настройки
	 * @param array $old_instance предыдущие настройки
	 *
	 * @return array данные которые будут сохранены
	 */
	function update( $new_instance, $old_instance ) {
		// $instance = array();
		// $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

		return $new_instance;
	}

}
// конец класса Foo_Widget

// регистрация Foo_Widget в WordPress
function register_top_pharmacies_widget() {
	register_widget( 'Top_Pharmacies_Widget' );
}
add_action( 'widgets_init', 'register_top_pharmacies_widget' );
