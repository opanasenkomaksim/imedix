<?php

/**
 * Добавление нового виджета Foo_Widget.
 */
class Interacting_Drugs_Widget extends WP_Widget {

    // Регистрация виджета используя основной класс
    function __construct() {
        // вызов конструктора выглядит так:
        // __construct( $id_base, $name, $widget_options = array(), $control_options = array() )
        parent::__construct('interacting_drugs', // ID виджета, если не указать (оставить ''), то ID будет равен названию класса в нижнем регистре
            '/*** STL - interacting drugs ***/', array('description' => 'Interacting drugs',
                                                       /*'classname' => 'my_widget',*/
            ));
    }

    /**
     * Вывод виджета во Фронт-энде
     *
     * @param array $args аргументы виджета.
     * @param array $instance сохраненные данные из настроек
     */
    function widget($args, $instance) {
        global $post;

        // Код виджета
        $options = array('orderby'        => array('date' => 'DESC'),
                         'posts_per_page' => 4,
        );

        $options['post_type'] = 'post';
        $options['cat']       = get_the_category()[0]->term_id;
        $options['post__not_in'] = array($post->ID);

        // d( get_the_category() );
        $loop = new WP_Query($options);
        ?>

        <?php if($loop->have_posts()) : ?>

            <?php
            $widget_id = $args['widget_id'];

            $title = apply_filters('widget_title', get_field('title', 'widget_' . $widget_id));

            echo $args['before_widget'];
            if(!empty($title)) {
                echo $args['before_title'] . $title . $args['after_title'];
            }
            ?>

            <div class="row">
                <?php while ($loop->have_posts()) : $loop->the_post(); ?>
                    <div class="col-lg-3 col-sm-6 colpadding" itemprop="relatedDrug" itemscope itemtype="http://schema.org/Drug">

                        <div class="thumbnail">
                            <?php if(has_post_thumbnail()) : ?>
                                <?php the_post_thumbnail('pharmacy_reviews', array('class' => 'article_image')); ?>
                            <?php else : ?>
                                <img src="<?php echo get_template_directory_uri(); ?>/production/images/pharmacy_reviews_thumbnail.png"
                                     srcset="<?php echo get_template_directory_uri(); ?>/production/images/pharmacy_reviews_thumbnail@2x.png 2x,
                                                <?php echo get_template_directory_uri(); ?>/production/images/pharmacy_reviews_thumbnail@3x.png 3x"
                                     class="article_image">
                            <?php endif; ?>
                        </div>

                        <div class="interacting-text">
                            <a href="<?php the_permalink(); ?>" itemprop="url">
                                <span itemprop="name"><?php the_title(); ?></span>
                            </a>
                        </div>

                        <?php if(!empty($verdict['active_ingredient']) && !empty($verdict['dosage'])) : ?>
                            <div class="interacting-discription">
                                    <span>
                                        (<?php echo $verdict['active_ingredient']; ?> <?php echo $verdict['dosage']; ?>)
                                    </span>
                            </div>
                        <?php endif; ?>

                    </div>
                <?php endwhile;
                wp_reset_query(); ?>
            </div>

            <?php echo $args['after_widget']; ?>

        <?php endif; ?>

        <?php
    }

    /**
     * Админ-часть виджета
     *
     * @param array $instance сохраненные данные из настроек
     */
    function form($instance) {
        ?>

        <?php
    }

    /**
     * Сохранение настроек виджета. Здесь данные должны быть очищены и возвращены для сохранения их в базу данных.
     *
     * @see WP_Widget::update()
     *
     * @param array $new_instance новые настройки
     * @param array $old_instance предыдущие настройки
     *
     * @return array данные которые будут сохранены
     */
    function update($new_instance, $old_instance) {
        // $instance = array();
        // $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

        return $new_instance;
    }

}

// конец класса Foo_Widget

// регистрация Foo_Widget в WordPress
function register_interacting_drugs_widget() {
    register_widget('Interacting_Drugs_Widget');
}

add_action('widgets_init', 'register_interacting_drugs_widget');
