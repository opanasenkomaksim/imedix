<?php
/**
 * Добавление нового виджета Foo_Widget.
 */
class Top_Users_Widget extends WP_Widget {

	// Регистрация виджета используя основной класс
	function __construct() {
		// вызов конструктора выглядит так:
		// __construct( $id_base, $name, $widget_options = array(), $control_options = array() )
		parent::__construct(
			'top_users', // ID виджета, если не указать (оставить ''), то ID будет равен названию класса в нижнем регистре
			'/*** STL - top users ***/',
			array( 'description' => 'Top users', /*'classname' => 'my_widget',*/ )
		);
	}

	/**
	 * Вывод виджета во Фронт-энде
	 *
	 * @param array $args     аргументы виджета.
	 * @param array $instance сохраненные данные из настроек
	 */
	function widget( $args, $instance ) {
		$widget_id = $args['widget_id'];

		$title = apply_filters( 'widget_title', get_field( 'title', 'widget_' . $widget_id ) );

		echo $args['before_widget'];
		if ( ! empty( $title ) ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}

		// Код виджета
		$count = get_field( 'count', 'widget_' . $widget_id );
		$options = array(
			'role__in' => array( 'administrator', 'editor', 'author', 'subscriber' ),
			'number' => $count,
			'orderby' => array ( 'meta_value_num' => 'DESC', 'date' => 'DESC' ),
			'meta_key' => 'user_score'
		);
		$loop = new WP_User_Query( $options );
		?>

		<ul class="list-unstyled" id="top_users">
			<?php $i = 1; foreach( $loop->get_results() as $user ) : ?>
			<li class="item">
				<?php
					$favorites_menu = get_current_user_id() != $user->ID;
				?>
				<div class="<?php if ( $favorites_menu ) echo 'open_favorites_menu'; ?> right">
					<a href="<?php echo get_author_posts_url($user->ID); ?>" class="item_link">
						<div class="user_position">
							<?php echo $i; ?>.
						</div>
						<div class="user_thumbnail">
							<?php echo get_avatar( $user->user_email, '36' ); ?>
						</div>
						<div class="user_name">
							<?php echo $user->display_name; ?>
						</div>
					</a>
					<?php if ( $favorites_menu ) : ?>
					<?php display_favorites_menu( 'user', $user->ID ); ?>
					<?php endif; ?>
				</div>

			</li>
			<?php $i++; endforeach; ?>
		</ul>

		<a href="<?php echo home_url( '/users/' ); ?>" class="more_link">
			All  Users <i class="fas fa-arrow-right"></i>
		</a>

		<?php

		echo $args['after_widget'];
	}

	/**
	 * Админ-часть виджета
	 *
	 * @param array $instance сохраненные данные из настроек
	 */
	function form( $instance ) {

		?>

		<?php
	}

	/**
	 * Сохранение настроек виджета. Здесь данные должны быть очищены и возвращены для сохранения их в базу данных.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance новые настройки
	 * @param array $old_instance предыдущие настройки
	 *
	 * @return array данные которые будут сохранены
	 */
	function update( $new_instance, $old_instance ) {
		// $instance = array();
		// $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

		return $new_instance;
	}

}
// конец класса Foo_Widget

// регистрация Foo_Widget в WordPress
function register_top_users_widget() {
	register_widget( 'Top_Users_Widget' );
}
add_action( 'widgets_init', 'register_top_users_widget' );
