<?php
/**
 * Добавление нового виджета Foo_Widget.
 */
class Content_Banner_Widget extends WP_Widget {

	// Регистрация виджета используя основной класс
	function __construct() {
		// вызов конструктора выглядит так:
		// __construct( $id_base, $name, $widget_options = array(), $control_options = array() )
		parent::__construct(
			'content_banner', // ID виджета, если не указать (оставить ''), то ID будет равен названию класса в нижнем регистре
			'/*** STL - content banner ***/',
			array( 'description' => 'Content banner', /*'classname' => 'my_widget',*/ )
		);
	}

	/**
	 * Вывод виджета во Фронт-энде
	 *
	 * @param array $args     аргументы виджета.
	 * @param array $instance сохраненные данные из настроек
	 */
	function widget( $args, $instance ) {
		$widget_id = $args['widget_id'];

		echo $args['before_widget'];

		// Код виджета
		$banner = get_field( 'banner', 'widget_' . $widget_id );
		$link = get_field( 'link', 'widget_' . $widget_id );
		// die(var_dump($banner));
		if( $banner ) :
		?>

		<div class="content_banner">
			<a href="<?php echo $link; ?>">
				<img src="<?php echo $banner[ 'sizes' ][ 'large' ]; ?>" class="banner_image">
			</a>
		</div>

		<?php
		endif;

		echo $args['after_widget'];
	}

	/**
	 * Админ-часть виджета
	 *
	 * @param array $instance сохраненные данные из настроек
	 */
	function form( $instance ) {

		?>

		<?php
	}

	/**
	 * Сохранение настроек виджета. Здесь данные должны быть очищены и возвращены для сохранения их в базу данных.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance новые настройки
	 * @param array $old_instance предыдущие настройки
	 *
	 * @return array данные которые будут сохранены
	 */
	function update( $new_instance, $old_instance ) {
		// $instance = array();
		// $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

		return $new_instance;
	}

}
// конец класса Foo_Widget

// регистрация Foo_Widget в WordPress
function register_content_banner_widget() {
	register_widget( 'Content_Banner_Widget' );
}
add_action( 'widgets_init', 'register_content_banner_widget' );
