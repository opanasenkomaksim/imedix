<?php
add_action( 'wp_ajax_ajax_email_notification_settings', 'ajax_email_notification_settings' ); // For logged in users
// add_action( 'wp_ajax_nopriv_ajax_email_notification_settings', 'ajax_email_notification_settings' ); // For anonymous users

function ajax_email_notification_settings(){
  // First check the nonce, if it fails the function will break
  check_ajax_referer( 'ajax-email-notification-settings', 'security' );

	$default = array(
		'new_question_notification' => false,
		'question_notification' => 'weekly',
		'new_follower_notification' => false,
		'new_answer_notification' => false,
		'new_comment_notification' => false,
		'new_upvote_notification' => false,

	);
	$options = array();
	foreach ( $default as $key => $value ) {
		$options[ $key ] = !empty( $_POST[ $key ] ) ? $_POST[ $key ] : $value;
	}

  $user_id = get_current_user_id();
 	if ( $user_id && update_user_meta( $user_id, 'notification_settings', $options ) ) {
	  echo json_encode(array('state'=>true));
  } else {
		echo json_encode(array('state'=>false, 'message'=> 'Settings can\'t be updated.'));
  }

  die();
}
