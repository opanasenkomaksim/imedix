<?php
add_action( 'wp_ajax_ajax_best_comment', 'ajax_best_comment' ); // For logged in users
// add_action( 'wp_ajax_nopriv_ajax_best_comment', 'ajax_best_comment' ); // For anonymous users

function ajax_best_comment(){
  // First check the nonce, if it fails the function will break
  check_ajax_referer( 'ajax-best-comment', 'security' );

  if( empty( $_POST[ 'submit' ] ) ) {
  	echo json_encode( array( 'state'=>false ) );
  	die();
  }

  if( $_POST[ 'submit' ] == 'yes' ) {
	  if( empty( $_POST[ 'post_id' ] ) || empty( $_POST[ 'comment_id' ] ) ) {
	  	echo json_encode( array( 'state'=>false ) );
	  	die();
	  }

	  $post_id = $_POST[ 'post_id' ];
	  $comment_id = $_POST[ 'comment_id' ];

		$args = array(
			'post_id' => $post_id,
			'parent' => 0,
		);

		if( $comments = get_comments( $args ) ){
			foreach( $comments as $comment ){
				update_field( 'best_answer_checked', true, 'comment_' . $comment->comment_ID );
			}
		}
		update_field( 'is_best_answer', true, 'comment_' . $comment_id );

		$post_data = array(
			'ID' => $post_id,
			'comment_status' => 'closed'
		);

		$post_id = wp_update_post( wp_slash( $post_data) );

  } else {
  	if( empty( $_POST[ 'comment_id' ] ) ) {
	  	echo json_encode( array( 'state'=>false ) );
	  	die();
	  }

  	$comment_id = $_POST[ 'comment_id' ];

  	update_field( 'best_answer_checked', true, 'comment_' . $comment_id );
  	update_field( 'is_best_answer', false, 'comment_' . $comment_id );

  }

  echo json_encode( array( 'state'=>true ) );
  die();
}
