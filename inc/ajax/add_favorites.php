<?php
add_action( 'wp_ajax_ajax_add_favorites', 'ajax_add_favorites' ); // For logged in users
// add_action( 'wp_ajax_nopriv_ajax_add_favorites', 'ajax_add_favorites' ); // For anonymous users

function ajax_add_favorites(){
  // First check the nonce, if it fails the function will break

	if( empty( $_POST[ 'category_ID' ] ) || empty( $_POST[ 'nonce' ] ) || ! wp_verify_nonce( $_POST[ 'nonce' ], 'ajax_add_favorites') ) {
		echo json_encode( array( 'state'=>false, 'message'=>'Can\'t add to favorites.' ) );
		die();
	}

	$user_id = get_current_user_id();
	if( empty( $user_id ) ) {
		echo json_encode( array( 'state'=>false, 'message'=>'Can\'t add to favorites.' ) );
		die();
	}

	$term_id = $_POST[ 'category_ID' ];


	$meta_key = add_user_meta( $user_id, 'favorite_categories', $term_id );

	$count_key = 'followers';
	$count = (int) get_term_meta( $term_id, $count_key, true );
  $count++;
  update_term_meta( $term_id, $count_key, $count );

	if( $meta_key ) {
		echo json_encode( array( 'state'=>true ) );
	} else {
		echo json_encode( array( 'state'=>false, 'message'=>'Can\'t add to favorites.' ) );
	}

  die();
}
