<?php
add_action( 'wp_ajax_ajax_edit_comment', 'ajax_edit_comment' ); // For logged in users
// add_action( 'wp_ajax_nopriv_ajax_edit_comment', 'ajax_edit_comment' ); // For anonymous users

function ajax_edit_comment(){
  // First check the nonce, if it fails the function will break
  check_ajax_referer( 'ajax-edit-comment', 'security' );

	if ( ! isset( $_POST[ 'comment_ID' ] ) || empty( $_POST[ 'comment' ] ) ) {
		echo json_encode( array( 'state'=>false, 'message'=>'Some reqiured fields are empty.' ) );
		die();
	}

	$comment_ID = (int) $_POST[ 'comment_ID' ];

	if ( get_comment( $comment_ID ) ) {
		$comment_data = array(
			'comment_ID' => $comment_ID,
			'comment_content' => wp_strip_all_tags( $_POST[ 'comment'] ),
		);

		$result = wp_update_comment( wp_slash( $comment_data) );
	} else {
		echo json_encode( array( 'state'=>false, 'message'=>'You can\'t edit this comment.' ) );
		die();
	}

	if ( $result && ! is_wp_error( $result ) ) {

		global $comment;
		$comment = get_comment( $comment_ID );
		$args = array(
				'type' => 'comment',
				'avatar_size' => 40
			);
		$depth = stl_get_comment_depth( $comment_ID );
		ob_start();
		custom_html5_comment( $comment, $args, $depth );
		$comment = ob_get_clean();

		echo json_encode( array( 'state'=>true, 'comment'=>$comment ) );
	} else {
		echo json_encode( array( 'state'=>false, 'message'=>'You can\'t edit this comment.' ) );
	}

  die();
}
