<?php
// add_action( 'wp_ajax_ajax_registration', 'ajax_registration' ); // For logged in users
add_action( 'wp_ajax_nopriv_ajax_registration', 'ajax_registration' ); // For anonymous users

function ajax_registration(){
  // First check the nonce, if it fails the function will break
  check_ajax_referer( 'ajax-registration', 'security' );

  // Nonce is checked, get the POST data and sign user on
  $info = array();
	$info['user_nicename'] = $info['nickname'] = $info['display_name'] = $info['first_name'] = sanitize_user( $_POST['name'] ) ;
	$info['last_name'] = sanitize_user($_POST['lastname']) ;
  $info['user_pass'] = sanitize_text_field($_POST['password']);
	$info['user_login'] = $info['user_email'] = sanitize_email( $_POST['email'] );

	// Register the user
  $user_id = wp_insert_user( $info );
 	if ( is_wp_error( $user_id ) ) {
		// $error  = $user_register->get_error_codes()	;
		$message  = $user_id->get_error_message()	;

		echo json_encode(array('state'=>false, 'message'=> $message));
  } else {
	  if( !send_confirm_email_key( $info['user_email'] ) ) {
	  	echo json_encode(array('state'=>false, 'message'=> __('Conformation letter wasn\'t send', 'imedix')));
	  } else {
	  	echo json_encode(array('state'=>true, 'email'=>$info['user_email']));
	  }
  }

  die();
}
