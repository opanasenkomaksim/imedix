<?php
add_action( 'wp_ajax_ajax_remove_question_from_favorites', 'ajax_remove_question_from_favorites' ); // For logged in users
// add_action( 'wp_ajax_nopriv_ajax_remove_question_from_favorites', 'ajax_remove_question_from_favorites' ); // For anonymous users

function ajax_remove_question_from_favorites(){
  // First check the nonce, if it fails the function will break

	if( empty( $_POST[ 'category_ID' ] ) || empty( $_POST[ 'nonce' ] ) || ! wp_verify_nonce( $_POST[ 'nonce' ], 'ajax_remove_question_from_favorites') ) {
		echo json_encode( array( 'state'=>false, 'message'=>'Can\'t remove from favorites.' ) );
		die();
	}

	$user_id = get_current_user_id();
	if( empty( $user_id ) ) {
		echo json_encode( array( 'state'=>false, 'message'=>'Can\'t remove from favorites.' ) );
		die();
	}

	$term_id = $_POST[ 'category_ID' ];

	if ( usermeta_value_exists( $user_id, 'favorite_categories', $term_id ) ) {
		$meta_key = delete_user_meta( $user_id, 'favorite_categories', $term_id );

		$count_key = 'followers';
		$count = (int) get_term_meta( $term_id, $count_key, true );
	  if ( $count > 0 ) $count--;
	  update_term_meta( $term_id, $count_key, $count );
	} else {
		$meta_key = true;
	}

	if( $meta_key ) {
		$favorites_menu = display_favorites_menu( 'category', $term_id, false );
		$favorites_button = display_favorites_button( 'category', $term_id, false );
		echo json_encode( array( 'state'=>true, 'favorites_menu'=>$favorites_menu, 'favorites_button'=>$favorites_button ) );
	} else {
		echo json_encode( array( 'state'=>false, 'message'=>'Can\'t remove from favorites.' ) );
	}

  die();
}
