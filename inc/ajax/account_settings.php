<?php
add_action('wp_ajax_ajax_account_settings', 'ajax_account_settings'); // For logged in users
// add_action( 'wp_ajax_nopriv_ajax_account_settings', 'ajax_account_settings' ); // For anonymous users

function ajax_account_settings()
{
    // First check the nonce, if it fails the function will break
    check_ajax_referer('ajax-account-settings', 'security');

    if ( empty($_POST['name']) || empty( $_POST['lastname'] ) ) {
        echo json_encode(array('state' => false, 'message' => 'Some reqiured fields are empty.'));
        die();
    }

    if ( isset( $_FILES['file'] ) ) {
        require_once( ABSPATH . 'wp-admin/includes/image.php' );
        require_once( ABSPATH . 'wp-admin/includes/file.php' );
        require_once( ABSPATH . 'wp-admin/includes/media.php' );

        $attachment_id = media_handle_upload( 'file', $_POST['post_id'] );

        if ( is_wp_error( $attachment_id ) ) {
            echo json_encode(array('state' => false, 'message' => 'File can\'t be uploaded.'));
            die();
        }
        global $wpdb;
        update_user_meta( get_current_user_id(), $wpdb->get_blog_prefix() . 'user_avatar', $attachment_id);
    }

    // Nonce is checked, get the POST data and sign user on
    $info = array();

    $info['ID'] = get_current_user_id();
    $info['user_nicename'] = $info['nickname'] = $info['display_name'] = $info['first_name'] = sanitize_user($_POST['name']);
    $info['last_name'] = sanitize_user($_POST['lastname']);

    // Register the user
    $user_id = wp_update_user($info);
    if (is_wp_error($user_id)) {
        // $error  = $user_register->get_error_codes()	;
        $message = $user_id->get_error_message();

        echo json_encode(array('state' => false, 'message' => $message));
    } else {
        echo json_encode(array('state' => true));
    }

    die();
}
