<?php
add_action( 'wp_ajax_ajax_remove_question', 'ajax_remove_question' ); // For logged in users
// add_action( 'wp_ajax_nopriv_ajax_remove_question', 'ajax_remove_question' ); // For anonymous users

function ajax_remove_question(){
  // First check the nonce, if it fails the function will break

	if( empty( $_POST[ 'post_id' ] ) || empty( $_POST[ 'nonce' ] ) || ! wp_verify_nonce( $_POST[ 'nonce' ], 'ajax_remove_question') ) {
		echo json_encode( array( 'state'=>false, 'message'=>'Can\'t remove post.' ) );
		die();
	}


	$post = get_post( $_POST[ 'post_id' ] );

	if( $post && wp_delete_post( $post->ID, true ) ) {
		echo json_encode( array( 'state'=>true, 'post_id'=>$post->ID, 'message'=>'Post successfully removed.' ) );
	} else {
		echo json_encode( array( 'state'=>false, 'message'=>'Can\'t remove post.' ) );
	}

  die();
}
