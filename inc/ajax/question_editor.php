<?php
add_action( 'wp_ajax_ajax_question_editor', 'ajax_question_editor' ); // For logged in users
// add_action( 'wp_ajax_nopriv_ajax_question_editor', 'ajax_question_editor' ); // For anonymous users

function ajax_question_editor(){
  // First check the nonce, if it fails the function will break
  check_ajax_referer( 'ajax-question-editor', 'security' );

	if ( ! isset( $_POST[ 'post_id' ] ) || ! isset( $_POST[ 'is_single' ] ) || empty( $_POST[ 'title' ] ) || empty( $_POST[ 'category' ] ) || empty( $_POST[ 'question_textarea' ] ) || ! is_array( $_POST[ 'category' ] ) ) {
		echo json_encode( array( 'state'=>false, 'message'=>'Some reqiured fields are empty.' ) );
		die();
	}

	if ( count( $_POST[ 'category' ] ) > 5  ) {
		echo json_encode( array( 'state'=>false, 'message'=>'You can insert maximum 5 categories.' ) );
		die();
	}

	$post_id = (int) $_POST[ 'post_id' ];

	if ( $post_id === 0 ) {
		$post_data = array(
			'post_title'    => wp_strip_all_tags( $_POST[ 'title'] ),
			'post_content'  => $_POST[ 'question_textarea' ],
			'post_status'   => 'pending',
			'post_author'   => get_current_user_id(),
			'post_category' => (array) $_POST[ 'category' ]
		);

		$post_id = wp_insert_post( $post_data );
		$message = 'Question successfully added';
	} else if ( get_post( $post_id ) ) {
		$post_data = array(
			'ID' => $post_id,
			'post_title' => wp_strip_all_tags( $_POST[ 'title'] ),
			'post_content' => $_POST[ 'question_textarea' ],
			'post_category' => (array) $_POST[ 'category' ],
			'post_status' => 'pending'
		);

		$post_id = wp_update_post( wp_slash( $post_data) );
		$message = 'Question successfully edited';
		// echo json_encode( array( 'state'=>false, 'message'=>'Test' ) );
		// die();
	} else {
		echo json_encode( array( 'state'=>false, 'message'=>'You can\'t edit this post.' ) );
		die();
	}

	if ( $post_id && ! is_wp_error( $post_id ) ) {
		global $post;
		$temp = $post;
		$post = get_post( $post_id );
		setup_postdata( $post );
		ob_start();

		if ( $_POST[ 'is_single' ] ) {
			include get_template_directory() . '/templates/question_body.php';
		} else {
			include get_template_directory() . '/templates/articles/article-any.php';
		}

		$article = ob_get_clean();
		wp_reset_postdata();
		$post = $temp;

		echo json_encode( array( 'state'=>true, 'post_id'=>$post_id, 'message'=>$message, 'article'=>$article ) );
	} else {
		echo json_encode( array( 'state'=>false, 'message'=>'Post can\'t be published.' ) );
	}

  die();
}
