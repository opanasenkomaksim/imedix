<?php
add_action( 'wp_ajax_ajax_read_notifications', 'ajax_read_notifications' ); // For logged in users
// add_action( 'wp_ajax_nopriv_ajax_read_notifications', 'ajax_read_notifications' ); // For anonymous users

function ajax_read_notifications(){
  // First check the nonce, if it fails the function will break

	if( empty( $_POST[ 'id' ] ) || !is_array( $_POST[ 'id' ] ) || empty( $_POST[ 'nonce' ] ) || !wp_verify_nonce( $_POST[ 'nonce' ], 'ajax_read_notifications') ) {
		echo json_encode( array( 'state'=>false, 'message'=>'Can\'t mark as readed.' ) );
		die();
	}

	$current_user_id = get_current_user_id();
	if( empty( $current_user_id ) ) {
		echo json_encode( array( 'state'=>false, 'message'=>'Can\'t mark as readed.' ) );
		die();
	}

	$ids = $_POST[ 'id' ];

    foreach ($ids as $id ) {
        notification_readed( $id );
    }

    echo json_encode( array( 'state'=>true ) );

    die();
}
