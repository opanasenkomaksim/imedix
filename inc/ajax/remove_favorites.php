<?php
add_action( 'wp_ajax_ajax_remove_question_from_favorites', 'ajax_remove_question_from_favorites' ); // For logged in users
// add_action( 'wp_ajax_nopriv_ajax_remove_question_from_favorites', 'ajax_remove_question_from_favorites' ); // For anonymous users

function ajax_remove_question_from_favorites(){
  // First check the nonce, if it fails the function will break

	if( empty( $_POST[ 'category_ID' ] ) || empty( $_POST[ 'nonce' ] ) || ! wp_verify_nonce( $_POST[ 'nonce' ], 'ajax_remove_question_from_favorites') ) {
		echo json_encode( array( 'state'=>false, 'message'=>'Can\'t remove to favorites.' ) );
		die();
	}

	$user_id = get_current_user_id();
	if( empty( $user_id ) ) {
		echo json_encode( array( 'state'=>false, 'message'=>'Can\'t remove to favorites.' ) );
		die();
	}

	$term_id = $_POST[ 'category_ID' ];


	$meta_key = delete_user_meta( $user_id, 'favorite_categories', $term_id );

	$count_key = 'followers';
	$count = (int) get_term_meta( $term_id, $count_key, true );
  if ( $count > 0 ) $count--;
  update_term_meta( $term_id, $count_key, $count );

	if( $meta_key ) {
		echo json_encode( array( 'state'=>true ) );
	} else {
		echo json_encode( array( 'state'=>false, 'message'=>'Can\'t remove to favorites.' ) );
	}

  die();
}
