<?php

function categories_filter() {
	global $wp_query;
	die('!!!');
	die(var_dump(is_front_page()));
	//gets the front page id set in options
	$is_front_page = get_option('page_on_front') == $query->query_vars['page_id'];

	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) return;
	if ( is_admin() ) return;
	if ( !$query->is_main_query( ) ) return;
	if ( !$is_front_page && !$query->is_category() ) return;


	if ( $filter == 'top-week' ) {
			function filter_where_date($where = '') {
		    //posts in the last 7 days
		    $where .= " AND post_date > '" . date('Y-m-d', strtotime('-7 days')) . "'";
		    return $where;
			}
			add_filter('posts_where', 'filter_where_date');
	}

	if ( $filter == 'top-month' ) {
		function filter_where_date($where = '') {
	    //posts in the last 30 days
	    $where .= " AND post_date > '" . date('Y-m-d', strtotime('-30 days')) . "'";
	    return $where;
		}
		add_filter('posts_where', 'filter_where_date');
	}

	if( is_user_logged_in() ) {
		function filter_where_status( $where ) {
	    $where .= " OR (post_author = " . get_current_user_id() . " AND
	    	post_status = 'pending'
	    ) ";
	    return $where;
		}
		add_filter('posts_where', 'filter_where_status');
	}

	// $paged = (get_query_var('page')) ? get_query_var('page') : 1;
	// $temp_wp_query = $wp_query;

	$options = array(
		'post_type' => 'post',
		// 'paged' => $paged,
		'orderby' => array ( 'date' => 'DESC' ),
		// 'posts_per_page' => 2,
	);

	if( is_front_page() ) {
		$options['category_name'] = 'qa';
	} else {
		$category = get_queried_object();
		$options['cat'] = $category->term_id;
	}

	if ( $filter == 'interesting' || $filter == 'top-week' || $filter == 'top-month' ) {
		$options['orderby'] = array ( 'meta_value_num' => 'DESC', 'date' => 'DESC' );
		$options['meta_key'] = 'post_rating';
	}

	$wp_query = new WP_Query( $options );

	if( $filter == 'top-week' || $filter == 'top-month' ) remove_filter( 'posts_where', 'filter_where_date' );
	if( is_user_logged_in() ) remove_filter( 'posts_where', 'filter_where_status' );

	// $query-> set('post_type' ,'page');
	// $query-> set('post__in' ,array( $front_page_id , [YOUR SECOND PAGE ID]  ));
	// $query-> set('orderby' ,'post__in');
	// $query-> set('p' , null);
	// $query-> set( 'page_id' ,null);
}
add_action( '__before_loop', 'categories_filter' );
