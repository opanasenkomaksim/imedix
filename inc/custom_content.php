<?php
function content_filter( $content ){
	// if( is_singular( 'pharmacies' ) ) {
	if ( is_category_post( 'drugs' ) || is_category_post( 'qa' ) || is_category_post( 'pharmacy-reviews' ) ) {

		$html = explode( '<h2', $content );
		$chapter = 1;
		foreach ( $html as $key => $content ) {
			if( ! empty( $content ) ) $content = '<h2' . $content;
			$html[ $key ] = array();

			preg_match( '/<h2[^>]*>(.*?)<\/h2>/i', $content, $match );
			// echo var_dump($match);
			if( ! empty( $match ) ) {
				$title = $match[1];
				$content = '<div id="chapter-' . $chapter . '">' . $content . '</div>';
				$html[ $key ][ 'chapter' ] = '<a href="#chapter-' . $chapter . '" class="animated-scroll item_link">' . $chapter . '. ' . $title . '</a>';

				$chapter++;
			}

			$html[ $key ][ 'content' ] = $content;
		}
		// die(var_dump($html));

		$new_html = '';
		if( $chapter > 1 ) {
			$new_html .= '
			<div class="row">
				<div class="col-12">
					<ol class="list-unstyled" id="chapters">';

					foreach ( $html as $key => $chapter_content ) {
						if( isset( $html[ $key ][ 'chapter' ] ) )
							$new_html .= '<li class="item">' . $html[ $key ][ 'chapter' ] . '</li>';
					}

			$new_html .= '
					</ol>
				</div>
			</div>';
		}

		foreach ( $html as $key => $chapter_content ) {
			if( isset( $html[ $key ] ) )
				$new_html .= image_wrap( $chapter_content[ 'content' ] );
		}

		return $new_html;
	}

	return $content;
}
add_filter( 'the_content', 'content_filter' );

function image_wrap( $content ) {
	preg_match_all( "/(<img\s[^>]*?src\s*=\s*['\"][^'\"]*?['\"][^>]*?>)/i", $content, $match );

	$images = array();
	foreach( $match[ 1 ] as $image) {
		preg_match( "/alt\s*=\s*['\"](.*?)['\"]/i", $content, $match2 );

		$description = ! empty( $match2 ) ? '<div class="description">' . $match2[1] . '</div>' : '';

		$content = str_replace($image, '<div class="image_wrapper">' . $image . $description . '</div>', $content );
	}

	return $content;
}
