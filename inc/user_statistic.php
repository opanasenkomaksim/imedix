<?php

function user_score_wp_insert_post( $post_id, $post, $update ) {
	if ( ! $update ) {
		$post = get_post( $post_id );
		if ( ! empty( $post ) && is_category_post( 'qa', $post_id ) ) {
			$questions_count = (int) get_user_meta( $post->post_author, 'questions_count', true );
			$questions_count++;
			update_user_meta( $post->post_author, 'questions_count', $questions_count );
			update_user_score( $post->post_author );
		}
	}
}
add_action( 'wp_insert_post', 'user_score_wp_insert_post', 10, 3 );

function user_score_pre_delete_post( $delete, $post, $force_delete ) {
	if ( $force_delete && is_category_post( 'qa', $post->ID ) ) {
		$questions_count = (int) get_user_meta( $post->post_author, 'questions_count', true );
		if ( $questions_count >= 1 ) $questions_count--;
		update_user_meta( $post->post_author, 'questions_count', $questions_count );
		update_user_score( $post->post_author );
	}

	return delete;
}
apply_filters( 'pre_delete_post', 'user_score_pre_delete_post', 10, 3 );

function user_score_wp_insert_comment( $comment_id, $comment ) {
    if ( $comment->comment_parent > 0 ) {
			$field = 'comments_count';
    } else {
			$field = 'answers_count';
    }

		$field_count = (int) get_user_meta( $comment->user_id, $field, true );
		$field_count++;
    update_user_meta( $comment->user_id, $field, $field_count );
    update_user_score( $comment->user_id );
}
add_action( 'wp_insert_comment', 'user_score_wp_insert_comment', 10, 2 );

function user_score_delete_comment( $comment_id, $comment ) {
	if ( in_array( wp_get_comment_status( $comment ), array( 'trash', 'spam' ) ) ) {
    if ( $comment->comment_parent > 0 ) {
			$field = 'comments_count';
    } else {
			$field = 'answers_count';
    }

		$field_count = (int) get_user_meta( $comment->user_id, $field, true );
		if ( $field_count >= 1 ) $field_count--;
		update_user_meta( $comment->user_id, $field, $field_count );
		update_user_score( $comment->user_id );
	}
}
add_action( 'delete_comment', 'user_score_delete_comment', 10, 2 );

function user_score_upvote_was_set( $user_id ) {
	$upvotes_count = (int) get_user_meta( $user_id, 'upvotes_count', true );
	$upvotes_count++;
	update_user_meta( $user_id, 'upvotes_count', $upvotes_count );
	update_user_score( $user_id );
}
add_action( 'upvote_was_set', 'user_score_upvote_was_set', 10, 1 );

function user_score_downvote_was_set( $user_id ) {
	$downvotes_count = (int) get_user_meta( $user_id, 'downvotes_count', true );
	$downvotes_count++;
	update_user_meta( $user_id, 'downvotes_count', $downvotes_count );
	update_user_score( $user_id );
}
add_action( 'downvote_was_set', 'user_score_downvote_was_set', 10, 1 );

function update_user_score( $user_id ) {
	$questions_count = (int) get_user_meta( $user_id, 'questions_count', true );
	$answers_count = (int) get_user_meta( $user_id, 'answers_count', true );
	$comments_count = (int) get_user_meta( $user_id, 'comments_count', true );
	$upvotes_count = (int) get_user_meta( $user_id, 'upvotes_count', true );

	$user_score = 10 * $questions_count + 5 * $answers_count + 3 * $comments_count + $upvotes_count;

	update_user_meta( $user_id, 'user_score', $user_score );
}
