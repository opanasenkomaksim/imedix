<?php
add_action( 'wp_ajax_ajax_restore_password', 'ajax_restore_password' ); // For logged in users
add_action( 'wp_ajax_nopriv_ajax_restore_password', 'ajax_restore_password' ); // For anonymous users

function ajax_restore_password(){
  // First check the nonce, if it fails the function will break
  check_ajax_referer( 'ajax-restore-password', 'security' );

  if( empty( $_POST[ 'email' ] ) ) {
  	echo json_encode( array( 'state'=>false, 'message'=>'Email can\'t be posted.' ) );
  	die();
  }

  $user = get_user_by( 'email', $_POST[ 'email' ] );

	if ( !$user ) {
		echo json_encode( array( 'state'=>false, 'message'=>'Email can\'t be posted.' ) );
		die();
	}

	$key = md5( microtime().rand() );
	update_user_meta( $user->ID, 'new_password_key', $key );
	$result = '<a href="' . home_url('/?action=new-password&nonce=' . wp_create_nonce('ajax_restore_password') . '&email=' . $_POST[ 'email' ]) . '&key=' . $key . '">Restore password link</a>';

	if( wp_mail( $_POST[ 'email' ], 'Restore your password, please', $result ) ) {
  	echo json_encode( array( 'state'=>true, 'message'=>'Email can\'t be posted.' ) );
	} else {
  	echo json_encode( array( 'state'=>false, 'message'=>'Email can\'t be posted.' ) );
	}

  die();
}

add_action( 'wp_ajax_ajax_new_password', 'ajax_new_password' ); // For logged in users
add_action( 'wp_ajax_nopriv_ajax_new_password', 'ajax_new_password' ); // For anonymous users

function ajax_new_password(){
  // First check the nonce, if it fails the function will break
  check_ajax_referer( 'ajax-new-password', 'security' );

	if ( empty( $_POST[ 'password' ] ) || ( !is_user_logged_in() && ( empty( $_POST[ 'nonce' ] ) || empty( $_POST[ 'email' ] ) || empty( $_POST[ 'key' ] ) || ! wp_verify_nonce( $_POST[ 'nonce' ], 'ajax_restore_password') ) ) ) {
		echo json_encode( array( 'state'=>false, 'message'=>'Password wasn\'t changed.' ) );
		die();
	}

	$password = $_POST[ 'password' ];

	$is_user_logged_in = false;
	if ( ! is_user_logged_in() ) {
		$email = $_POST[ 'email' ];
		$key = $_POST[ 'key' ];

	  $user = get_user_by( 'email', $email );

		if ( empty( $user ) ) {
			$key_value = get_user_meta( $user->ID, 'new_password_key', true );
			if( $key != $key_value ) {
				echo json_encode( array( 'state'=>false, 'message'=>'Password wasn\'t changed.' ) );
				die();
			}
		}
	} else {
		$user = wp_get_current_user();
		$is_user_logged_in = true;
	}

	if ( ! empty( $user ) ) {
		wp_set_password( $password, $user->ID );
		delete_user_meta( $user->ID, 'new_password_key' );
		$user_login = $user->user_email;
		$user_login = $user->user_email;
		$result = auth_user_login( $user_login, $password, '' );
		if ( $result[ 'loggedin' ] == true ) {
			echo json_encode( array( 'state'=>true, 'message'=>'Password was changed.' ) );
		} else {
			echo json_encode( array( 'state'=>true, 'message'=>'Password was changed. Try to login.' ) );
		}
		die();
	}

	echo json_encode( array( 'state'=>false, 'message'=>'Password wasn\'t changed.' ) );
  die();
}

function new_password() {
	if( empty( $_GET[ 'action' ] ) || $_GET[ 'action' ] != 'new-password' || empty( $_GET[ 'nonce' ] ) || empty( $_GET[ 'key' ] ) || empty( $_GET[ 'email' ] ) ) return;

	$email = $_GET[ 'email' ];
	$key = $_GET[ 'key' ];

 	$user = get_user_by( 'email', $email );

	if( ! wp_verify_nonce( $_GET[ 'nonce' ], 'ajax_restore_password') && $user->ID != get_current_user_id() ) return;

	if ( $user ) {
		$key_value = get_user_meta( $user->ID, 'new_password_key', true );
		if( $key == $key_value ) {

			echo <<<START
				<script>
				jQuery(document).ready(function($) {
					$.fancybox.open({
						src : '#new_password_popup'
					});
				});
				</script>
START;

		}
	}
}
add_action('wp_footer', 'new_password', 20);

function func_wp_new_user_notification_email( $wp_new_user_notification_email, $user_data, $blogname ) {
	$key = md5( microtime().rand() );
	update_user_meta( $user_data->ID, 'new_password_key', $key );
	$message = '<a href="' . home_url('/?action=new-password&nonce=' . wp_create_nonce('ajax_restore_password') . '&email=' . $user_data->user_email ) . '&key=' . $key . '">Restore password link</a>';

	$wp_new_user_notification_email = array(
		'to'      => $user_data->user_email,
		/* translators: Password change notification email subject. %s: Site title */
		'subject' => __( '[%s] Your username and password info' ),
		'message' => $message,
		'headers' => '',
	);
	return $wp_new_user_notification_email;
}
add_filter( 'wp_new_user_notification_email', 'func_wp_new_user_notification_email', 10, 3 );

function func_retrieve_password_message( $message, $key, $user_login, $user_data ) {
	$key = md5( microtime().rand() );
	update_user_meta( $user_data->ID, 'new_password_key', $key );
	$message = '<a href="' . home_url('/?action=new-password&nonce=' . wp_create_nonce('ajax_restore_password') . '&email=' . $user_data->user_email ) . '&key=' . $key . '">Restore password link</a>';

	return $message;
}
add_filter( 'retrieve_password_message', 'func_retrieve_password_message', 10, 4 );
