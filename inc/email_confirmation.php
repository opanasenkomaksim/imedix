<?php
function filter_authenticate( $user, $password ) {
	$email_confirmed = get_user_meta( $user->ID, 'email_confirmed', true );

	if( ! $email_confirmed ) {
		return new WP_Error( 'email_is_not_confirmed',
			sprintf( __( 'Email is not confirmed. <a href="#" class="email_confirmation_link" data-email="%s">Please, confirm your email.</a>', 'imedix' ), $user->user_email )
		);
	}

	return $user;
}
add_filter( 'wp_authenticate_user', 'filter_authenticate', 10, 2 );

add_action( 'wp_ajax_ajax_email_confirmation', 'ajax_email_confirmation' ); // For logged in users
add_action( 'wp_ajax_nopriv_ajax_email_confirmation', 'ajax_email_confirmation' ); // For anonymous users

function ajax_email_confirmation(){
  if( empty( $_POST[ 'email' ] ) ) {
  	echo json_encode( array( 'state'=>false, 'message'=>'Email is not given.' ) );
  	die();
  }

  $user = get_user_by( 'email', $_POST[ 'email' ] );

	if ( !$user ) {
  	echo json_encode( array( 'state'=>false, 'message'=>'There is no user with such email.' ) );
  	die();
	}

	$key = md5( microtime().rand() );
	update_user_meta( $user->ID, 'email_confirmation_key', $key );
	$result = '<a href="' . home_url('/?action=confirm-email&nonce=' . wp_create_nonce('ajax_email_confirmation') . '&email=' . $_POST[ 'email' ] . '&key=' . $key) . '">Confirm your email link</a>';

	if( wp_mail( $_POST[ 'email' ], 'Confirm your email, please', $result ) ) {
  	echo json_encode( array( 'state'=>true ) );
	} else {
  	echo json_encode( array( 'state'=>false, 'message'=>'Email can\'t be posted.' ) );
	}

	die();
}

function send_confirm_email_key( $email = false ) {
	if ( !$email ) return false;

	$user = get_user_by( 'email', $email );

	if ( !$user ) return false;

	$key = md5( microtime().rand() );
	update_user_meta( $user->ID, 'email_confirmation_key', $key );
	$result = '<a href="' . home_url('/?action=confirm-email&nonce=' . wp_create_nonce('ajax_email_confirmation') . '&email=' . $email . '&key=' . $key) . '">Confirm your email link</a>';

	if( wp_mail( $email, 'Confirm your email, please', $result ) ) {
		return true;
	} else {
		return false;
	}
}

function confirm_email() {
	if( empty( $_GET[ 'action' ] ) || $_GET[ 'action' ] != 'confirm-email' || empty( $_GET[ 'nonce' ] ) || empty( $_GET[ 'email' ] ) || empty( $_GET[ 'key' ] ) || ! wp_verify_nonce( $_GET[ 'nonce' ], 'ajax_email_confirmation') ) return;

	$email = $_GET[ 'email' ];
	$key = $_GET[ 'key' ];

  $user = get_user_by( 'email', $email );

	if ( $user ) {
		$key_value = get_user_meta( $user->ID, 'email_confirmation_key', true );
		if( $key == $key_value ) {
			update_user_meta( $user->ID, 'email_confirmed', true );
			delete_user_meta( $user->ID, 'email_confirmation_key' );

			echo <<<START
				<script>
				jQuery(document).ready(function($) {
					$.fancybox.open({
						src : '#email_confirmed_popup'
					});
				});
				</script>
START;

		}
	}
}
add_action('wp_footer', 'confirm_email', 20);
