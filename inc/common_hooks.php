<?php
/**
 * Created by seattleby.com
 * User: Maksim Apanasenka
 * Date: 12/27/2018
 * Time: 8:43 AM
 */

function change_from_mail( $email_address ){
    return 'noreply@' . get_domain();
}
add_filter( 'wp_mail_from', 'change_from_mail' );

function change_from_name( $email_from ){
    return 'iMedix';
}
add_filter( 'wp_mail_from_name', 'change_from_name' );
