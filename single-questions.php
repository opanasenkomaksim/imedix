<?php get_header(); ?>

<main itemscope itemtype="http://schema.org/QAPage">
	<div class="container" itemprop="mainEntity" itemprop="acceptedAnswer" itemscope itemtype="http://schema.org/Question">

		<ol class="list-unstyled" id="breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
		  <li class="item" itemprop="itemListElement" itemscope
		      itemtype="http://schema.org/ListItem">
		    <a itemprop="item" href="<?php echo home_url(); ?>">
		    <span itemprop="name">HOME</span></a>
		    <meta itemprop="position" content="1" />
		  </li>
		  <li class="item" itemprop="itemListElement" itemscope
		      itemtype="http://schema.org/ListItem">
		    <a itemprop="item" href="<?php echo home_url( '/category/qa/' ); ?>">
		    <span itemprop="name">QUESTIONS</span></a>
		    <meta itemprop="position" content="2" />
		  </li>
		</ol>

		<div class="row">
			<div class="col-lg-9">

			<?php if (have_posts()) : ?>
	    	<?php while (have_posts()) : the_post(); ?>

				<article id="article-any">

					<?php include get_template_directory() . '/templates/question_menu.php'; ?>

					<?php include get_template_directory() . '/templates/question_body.php'; ?>

					<div class="row">
						<div class="col-12 widgets-col">
							<?php
								if ( ! dynamic_sidebar('question_post_content') ) _e('Add widgets to sidebar', 'imedix');
							?>
						</div>
					</div>

					<div class="row popularity-col">
						<div class="col item">
							<img src="<?php echo get_template_directory_uri(); ?>/production/images/comments_icon.png"
								 srcset="<?php echo get_template_directory_uri(); ?>/production/images/comments_icon@2x.png 2x,
										 <?php echo get_template_directory_uri(); ?>/production/images/comments_icon@3x.png 3x"
								 class="comments_image">
							<span class="text">
								<span class="description">Respond: </span><span itemprop="answerCount"><?php echo get_comments_number(); ?></span>
							</span>
						</div>
						<div class="col-auto item">
							<img src="<?php echo get_template_directory_uri(); ?>/production/images/views_icon.png"
								 srcset="<?php echo get_template_directory_uri(); ?>/production/images/views_icon@2x.png 2x,
										 <?php echo get_template_directory_uri(); ?>/production/images/views_icon@3x.png 3x"
								 class="comments_image">
							<span class="text">
								<span class="description">Views: </span><?php display_views(); ?>
							</span>
						</div>
					</div>

                    <?php comments_template(); ?>
				</article>

				<?php endwhile; wp_reset_query(); ?>
			<?php endif; ?>

			</div>
			<aside class="col-lg-3 sidebar-col d-none d-lg-block">
				<?php
					if ( ! dynamic_sidebar('question_post_sidebar') ) _e('Add widgets to sidebar', 'imedix');
				?>
			</aside>
		</div>
	</div>
</main>

<?php get_footer(); ?>
