<?php get_header(); ?>

<main>
	<div class="container">
		<div class="row">
			<aside class="col-lg-3 sidebar-settings d-none d-lg-block">

				<?php include get_template_directory() . '/templates/settings_sidebar.php'; ?>

			</aside>
			<div class="col-lg-9">
				<div class="content-settings favorite_categories">
					<div class="content">
						<?php
							$categories = get_usermeta_values( get_current_user_id(), 'favorite_categories', 30 );
							$total = $categories->max_num_pages;
							$found_meta = $categories->found_meta;
							$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
						?>
						<h1 class="page_title">
							<?php echo $found_meta; ?> Favorite Categories
						</h1>
						<?php $count = count( $categories->meta ); ?>
						<?php $i = 1; foreach ( $categories->meta as $meta ) : ?>
						<?php $category = get_category( $meta->meta_value ); ?>
						<?php if ( $i % 2 == 1 ) : ?>
						<div class="row">
						<?php endif; ?>
							<div class="col-12 col-sm-6 item-col">
								<div class="item">
									<?php echo $category->name; ?>
									<a href="<?php echo $category->term_id; ?>" class="remove_question_from_favorites_link" data-nonce="<?php echo wp_create_nonce('ajax_remove_question_from_favorites'); ?>">
										<i class="far fa-times"></i>
									</a>
								</div>
							</div>
						<?php if ( $i % 2 == 0 || $i == $count ) : ?>
						</div>
						<?php endif; ?>
						<?php $i++; endforeach; ?>
					</div>
					<div class="row">
						<div class="col">
						<?php
						  $paginate = paginate_links(array(
									// 'total' => 8,
									// 'current' => 1,
						      'total' => $total,
						      'current' => $paged,
						      // 'base' => URI . '/my-account/manage-tours/' . '%_%',
						      // 'format' => '%#%',
						      'type' => 'array',
						      'end_size' => 1,
						      'mid_size' => 1,
						      'prev_text' => '<i class="fas fa-caret-left"></i>',
						      'next_text' => '<i class="fas fa-caret-right"></i>',
						  ));
						  // die(var_dump($paginate));
							display_pagination( $paginate );
						?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>

<?php get_footer(); ?>
